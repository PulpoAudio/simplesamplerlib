/******************************************************************
 This software has been developed 2017 by Rudi Leitner
 as open source under the GPL3 license.
 
 It uses the open source version of JUCE GRAPEFRUIT under GPL3.
 *******************************************************************/
//
//  UIControls.hpp
//  SimpleSamplerComponent
//
//  Created by Rudolf Leitner on 11/12/16.
//
//

#ifndef UIControls_hpp
#define UIControls_hpp

#include <stdio.h>
#include <vector>
#include "../JuceLibraryCode/JuceHeader.h"


/*--------------------------------------------------------------------------------------------------------------
 * KnobManSlider
 *
 *
 *
 ---------------------------------------------------------------------------------------------------------------*/
class KnobManImage
{
	public:
		KnobManImage (Image * aKnobManImg, int aWidth, int aHeight, int aCount);
		void drawFrame (Graphics &g, int x, int y, int width, int height,  float imgNdx);

	Image *knobManImg;
	int count;
	int width;
    int height;
	bool vertical;
};



/*--------------------------------------------------------------------------------------------------------------
 * MyLookAndFeel
 *
 *
 *
 ---------------------------------------------------------------------------------------------------------------*/
class MyLookAndFeel : public LookAndFeel_V3
{

private:
	KnobManImage* rotarySliderKnobManImg;
    KnobManImage* toggleButtonKnobManImg;

public:
    MyLookAndFeel();

	virtual ~MyLookAndFeel();

	void setRotarySliderKnobManImage( Image *aRotarySliderKnobManImg, int _frameWidth, int _frameHeight, int _frameCount  );
    void setToggleButtonKnobManImage( Image *aToggleSliderKnobManImg, int _frameWidth, int _frameHeight, int _frameCount  );

	Slider::SliderLayout getSliderLayout (Slider&) override
	{
		Slider::SliderLayout layout;

		layout.sliderBounds = Rectangle<int> (16,0, 32, 32);
		layout.textBoxBounds = Rectangle<int> (0, 33, 60, 14);


		return layout;
	}
    
    void drawRotarySlider (Graphics&, int x, int y, int width, int height,
                           float sliderPosProportional, float rotaryStartAngle, float rotaryEndAngle,
                           Slider&) override;
    
    void drawToggleButton 	(Graphics& g,ToggleButton& tb ,bool isMouseOverButton, bool isButtonDown ) override;
    
};






/*--------------------------------------------------------------------------------------------------------------
 * TimerUpdateableComponent
 *
 *
 *
 ---------------------------------------------------------------------------------------------------------------*/

class UIUpdateTimer;

class TimerUpdateableComponent
{
protected:
    UIUpdateTimer* uiUpdateTimer;
public:
    TimerUpdateableComponent(UIUpdateTimer* aUIUpdateTimer)
    {
        uiUpdateTimer = aUIUpdateTimer;
    }

    virtual ~TimerUpdateableComponent()
    {
    };

    virtual void updateComponent() {};
};



/*--------------------------------------------------------------------------------------------------------------
 * UIUpdateTimer
 *
 *
 *
 ---------------------------------------------------------------------------------------------------------------*/
class UIUpdateTimer : public Timer
{
private:
	std::vector<TimerUpdateableComponent*> components;

public:
    UIUpdateTimer()
    : Timer()
    {
    this->startTimerHz(30);
    }


	virtual ~UIUpdateTimer()
	{
		this->stopTimer();
	};


	void registerComponent( TimerUpdateableComponent* aComponent) {
		components.push_back(aComponent);
	}

	void unregisterComponent( TimerUpdateableComponent* aComponent) {
		for( std::vector<TimerUpdateableComponent*>::iterator it = components.begin(); it != components.end(); it++) {
			if( *it == aComponent ) {
				components.erase( it );
				break;
			}
		}
	}

	void timerCallback() override
	{
		for( std::vector<TimerUpdateableComponent*>::iterator it = components.begin(); it != components.end(); it++) {
			(*it)->updateComponent();
		}
	}
};



/*--------------------------------------------------------------------------------------------------------------
 * DBParameterSlider
 *
 *
 *
 ---------------------------------------------------------------------------------------------------------------*/
class DBParameterSlider :  public Slider, public TimerUpdateableComponent {
public:
	DBParameterSlider (LookAndFeel* aLookAndFeel, UIUpdateTimer* aUIUpdateTimer, AudioProcessorParameter& p, float aSkewFactor)
	: Slider (p.getName (256)), TimerUpdateableComponent(aUIUpdateTimer), param (p)
	{
		setLookAndFeel(aLookAndFeel);
		setTextValueSuffix(" dB");
		setSkewFactor(aSkewFactor, (aSkewFactor == 1.0f));
		// TODO:    see how skewfactor can be calculated for other db ranges  (current: -60 to +12)

		setRange (0.0, 1.0, 0.0);
		updateComponent();
		setSliderStyle (Slider::Rotary);
        aUIUpdateTimer->registerComponent( this );
	}

	virtual ~DBParameterSlider()
	{
		uiUpdateTimer->unregisterComponent(this);
	}

	virtual void valueChanged() override
	{
		if (isMouseButtonDown()) {
			param.setValueNotifyingHost ((float) Slider::getValue());
		} else {
			param.setValue ((float) Slider::getValue());
		}
	}


	void startedDragging() override     { param.beginChangeGesture(); }
	void stoppedDragging() override     { param.endChangeGesture();   }

	virtual double getValueFromText (const String& text) override
	{
		AudioParameterFloat& fParam = (AudioParameterFloat&) param;
		String t (text.trimStart());

		if (t.endsWith (getTextValueSuffix())) {
			t = t.substring (0, t.length() - getTextValueSuffix().length());
		}

		while (t.startsWithChar ('+')) {
			t = t.substring (1).trimStart();
		}

		float vdb = t.getFloatValue();
		float vgain = Decibels::decibelsToGain(vdb);
		float normalValue = fParam.range.convertTo0to1(vgain);
		return normalValue;
	}


	virtual String getTextFromValue (double value) override
	{
		AudioParameterFloat& fParam = (AudioParameterFloat&) param;

		float unnormValue = fParam.range.convertFrom0to1(value);
		float vdb = Decibels::gainToDecibels(unnormValue);
		String svdb (vdb,2);
		svdb += getTextValueSuffix();
		return svdb;
	}

	virtual void updateComponent() override
	{
		const float newValue = param.getValue();

		if (newValue != (float) Slider::getValue() && ! isMouseButtonDown()) {
			Slider::setValue (newValue);
		}
	}

	AudioProcessorParameter& param;


	JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (DBParameterSlider)
};


/*--------------------------------------------------------------------------------------------------------------
 * ParameterSlider
 *
 *
 *
 ---------------------------------------------------------------------------------------------------------------*/
class ParameterSlider   : public Slider, public TimerUpdateableComponent {
public:


	ParameterSlider (LookAndFeel* aLookAndFeel, UIUpdateTimer* aUIUpdateTimer,AudioProcessorParameter& p, std::string aTextValueSuffix = "", float aSkewFactor = 1.0f)
	: Slider(p.getName(256)), TimerUpdateableComponent(aUIUpdateTimer), param(p)
	{
        setLookAndFeel(aLookAndFeel);
		setTextValueSuffix(aTextValueSuffix);
		setRange (0.0, 1.0, 0.0);
		updateComponent();
		setSliderStyle (Slider::Rotary);
		setSkewFactor( aSkewFactor, (aSkewFactor == 1.0f));
        aUIUpdateTimer->registerComponent( this );
	}

	virtual ~ParameterSlider()
	{
		uiUpdateTimer->unregisterComponent(this);
	}


	virtual void valueChanged() override
	{
		if (isMouseButtonDown())
			param.setValueNotifyingHost ((float) Slider::getValue());
		else
			param.setValue ((float) Slider::getValue());
	}


	void startedDragging() override     { param.beginChangeGesture(); }
	void stoppedDragging() override     { param.endChangeGesture();   }

	virtual double getValueFromText (const String& text) override
	{
		String t (text.trimStart());

		if (t.endsWith (getTextValueSuffix())) {
			t = t.substring (0, t.length() - getTextValueSuffix().length());
		}

		while (t.startsWithChar ('+')) {
			t = t.substring (1).trimStart();
		}
		return param.getValueForText (t);
	}

	virtual String getTextFromValue (double value) override
	{
		return param.getText ((float) value, 1024) + getTextValueSuffix();
	}

	virtual void updateComponent() override
	{
		const float newValue = param.getValue();

		if (newValue != (float) Slider::getValue() && ! isMouseButtonDown()) {
			Slider::setValue (newValue);
		}
	}

	AudioProcessorParameter& param;


	JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (ParameterSlider)
};


/*--------------------------------------------------------------------------------------------------------------
 * PrcParameterSlider
 *
 *
 *
 ---------------------------------------------------------------------------------------------------------------*/
class PrcParameterSlider :  public Slider, TimerUpdateableComponent {
public:
	PrcParameterSlider (LookAndFeel* aLookAndFeel, UIUpdateTimer* aUIUpdateTimer,AudioProcessorParameter& p, float aSkewFactor)
	: Slider(p.getName (256)), TimerUpdateableComponent(aUIUpdateTimer), param (p)
	{
        setLookAndFeel(aLookAndFeel);
		setTextValueSuffix(" %");

		setRange (0.0, 1.0, 0.0);
		updateComponent();
		setSliderStyle (Slider::Rotary);
		setSkewFactor( aSkewFactor, (aSkewFactor == 1.0f));
        aUIUpdateTimer->registerComponent( this );
	}

	virtual ~PrcParameterSlider()
	{
		uiUpdateTimer->unregisterComponent(this);
	}


	virtual void valueChanged() override
	{
		if (isMouseButtonDown()) {
			param.setValueNotifyingHost ((float) Slider::getValue());
		} else {
			param.setValue ((float) Slider::getValue());
		}
	}

	void startedDragging() override     { param.beginChangeGesture(); }
	void stoppedDragging() override     { param.endChangeGesture();   }

	virtual double getValueFromText (const String& text) override
	{
		AudioParameterFloat& fParam = (AudioParameterFloat&) param;
		String t (text.trimStart());

		if (t.endsWith (getTextValueSuffix())) {
			t = t.substring (0, t.length() - getTextValueSuffix().length());
		}

		while (t.startsWithChar ('+')) {
			t = t.substring (1).trimStart();
		}

		float vPrc = t.getFloatValue();
		float vNorm = vPrc / 100.0f;
		float normalValue = fParam.range.convertTo0to1(vNorm);
		return normalValue;
	}


	virtual String getTextFromValue (double value) override
	{
		AudioParameterFloat& fParam = (AudioParameterFloat&) param;

		float unnormValue = fParam.range.convertFrom0to1(value);
		float vPrc = unnormValue * 100.0f;
		String svprc (vPrc,2);
		svprc += getTextValueSuffix();
		return svprc;
	}

	virtual void updateComponent() override
	{
		const float newValue = param.getValue();

		if (newValue != (float) Slider::getValue() && ! isMouseButtonDown()) {
			Slider::setValue (newValue);
		}
	}

	AudioProcessorParameter& param;


	JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (PrcParameterSlider)
};


/*--------------------------------------------------------------------------------------------------------------
 * ParameterChoiceComboBox
 *
 *
 *
 ---------------------------------------------------------------------------------------------------------------*/
class ParameterChoiceComboBox : public ComboBox, public TimerUpdateableComponent {
public:
	ParameterChoiceComboBox(LookAndFeel* aLookAndFeel, UIUpdateTimer* aUIUpdateTimer,AudioParameterChoice& p)
	: ComboBox(), TimerUpdateableComponent(aUIUpdateTimer), param(p)
	{
        setLookAndFeel(aLookAndFeel);
		for (int i = 0; i < param.choices.size(); ++i)
			addItem(param.choices[i], i + 1);
        aUIUpdateTimer->registerComponent( this );
	}

	virtual ~ParameterChoiceComboBox()
	{
		uiUpdateTimer->unregisterComponent(this);
	}


	void valueChanged(Value& v) override {
		ComboBox::valueChanged(v);
		param.setValueNotifyingHost((float)(ComboBox::getSelectedId() - 1) / (float)(param.choices.size() - 1));
	}

	virtual void updateComponent() override
	{
		if (param != ComboBox::getSelectedId() - 1 && ! ComboBox::isPopupActive()) {
			ComboBox::setSelectedId(param + 1, NotificationType::dontSendNotification);
		}
	}

	AudioParameterChoice& param;

	JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(ParameterChoiceComboBox)
};


class ParameterIntComboBox : public ComboBox, public TimerUpdateableComponent {
public:
	ParameterIntComboBox(LookAndFeel* aLookAndFeel, UIUpdateTimer* aUIUpdateTimer,AudioParameterInt& p)
	: ComboBox(), TimerUpdateableComponent(aUIUpdateTimer), param(p)
	{
        setLookAndFeel(aLookAndFeel);
		//for (int i = 0; i < param.choices.size(); ++i)
		//	addItem(param.choices[i], i + 1);
        aUIUpdateTimer->registerComponent( this );
        numChoices = 0;
	}

	virtual ~ParameterIntComboBox()
	{
		uiUpdateTimer->unregisterComponent(this);
	}

	void setSelectionTexts( StringArray& texts )
	{
		clear( NotificationType::dontSendNotification);
        numChoices = texts.size();
		addItemList( texts,1);
	}


	void valueChanged(Value& v) override {
		ComboBox::valueChanged(v);
		param.setValueNotifyingHost((float)(ComboBox::getSelectedId() - 1)  / (float)(param.getRange().getEnd() - 1));
        
	}

	virtual void updateComponent() override
	{
		if (param != ComboBox::getSelectedId() - 1 && ! ComboBox::isPopupActive()) {
			ComboBox::setSelectedId(param + 1, NotificationType::dontSendNotification);
		}
	}

	AudioParameterInt& param;
    int numChoices;

	JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(ParameterIntComboBox)
};




/*--------------------------------------------------------------------------------------------------------------
 * ParameterChoiceComboBox
 *
 *
 *
 ---------------------------------------------------------------------------------------------------------------*/
class ParameterToggleButton : public ToggleButton, public TimerUpdateableComponent {
public:
	ParameterToggleButton(LookAndFeel* aLookAndFeel, UIUpdateTimer* aUIUpdateTimer,AudioParameterBool& p)
	: ToggleButton(""), TimerUpdateableComponent(aUIUpdateTimer), param(p)
	{
        setLookAndFeel(aLookAndFeel);
		aUIUpdateTimer->registerComponent( this );
	}

	virtual ~ParameterToggleButton()
	{
		uiUpdateTimer->unregisterComponent(this);
	}

	void buttonStateChanged() override {
		ToggleButton::buttonStateChanged();
		param.setValueNotifyingHost(getToggleState());
	}

	virtual void updateComponent() override
	{
        if( param != getToggleState() && !isMouseButtonDown() ) {
			setToggleState( param,NotificationType::dontSendNotification );
		}
	}

	AudioParameterBool& param;

	JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(ParameterToggleButton)
};



/*--------------------------------------------------------------------------------------------------------------
 * KeyRangeMidiKeyboardComponent
 *
 *
 *
 ---------------------------------------------------------------------------------------------------------------*/
class KeyRangeMidiKeyboardComponent : public MidiKeyboardComponent {
public:


    enum RangeColours
    {
    	playRangeColour = 0xff00ff00,
		keyswitchRangeColour = 0xff0000ff
    };

    int playRangeFromNote;
    int playRangeToNote;
    int playRangeShift;
    int keyswitchRangeFromNote;
    int keyswitchRangeToNote;
    int keyswitchRangeShift;


    KeyRangeMidiKeyboardComponent (MidiKeyboardState& state,
                           Orientation orientation);


    void setPlayRange( int aRangeFromNote, int aRangeToNote);
    void setKeyswitchRange( int aRangeFromNote, int aRangeToNote);
    void setPlayRangeShift( int aShift );
    void setKeyswitchRangeShift( int aShift );

    int getPlayRangeShift() { return playRangeShift;};
    int getKeyswitchRangeShift() { return keyswitchRangeShift;};
    void centerVisibleRange();

protected:

    bool isKeyswitchNote( int midiNoteNumber );
    bool isPlayNote( int midiNoteNumber);

    virtual void drawWhiteNote (int midiNoteNumber,
                                Graphics& g,
                                int x, int y, int w, int h,
                                bool isDown, bool isOver,
                                const Colour& lineColour,
                                const Colour& textColour) override;

    virtual void drawBlackNote (int midiNoteNumber,
                                Graphics& g,
                                int x, int y, int w, int h,
                                bool isDown, bool isOver,
                                const Colour& noteFillColour) override;



	JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(KeyRangeMidiKeyboardComponent)

};

/*--------------------------------------------------------------------------------------------------------------
 * ParameterChoiceComboBox
 *
 *
 *
 ---------------------------------------------------------------------------------------------------------------*/
class ParameterKeyRangeMidiKeyboardComponent : public KeyRangeMidiKeyboardComponent, public TimerUpdateableComponent {
public:
	ParameterKeyRangeMidiKeyboardComponent(UIUpdateTimer* aUIUpdateTimer, MidiKeyboardState& state, Orientation orientation, AudioParameterInt& p1, AudioParameterInt& p2)
	: KeyRangeMidiKeyboardComponent(state, orientation), TimerUpdateableComponent(aUIUpdateTimer), param1(p1), param2(p2)
	{
		aUIUpdateTimer->registerComponent( this );
	}
    
	virtual ~ParameterKeyRangeMidiKeyboardComponent()
	{
		uiUpdateTimer->unregisterComponent(this);
	}

	virtual void updateComponent() override
	{
		if( param1 != getPlayRangeShift()) {
			setPlayRangeShift( param1);
            centerVisibleRange();
			repaint();
		}
		if( param2 != getKeyswitchRangeShift() ) {
			setKeyswitchRangeShift( param2);
            centerVisibleRange();
            repaint();
		}
	}

	AudioParameterInt& param1;
    AudioParameterInt& param2;

	JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(ParameterKeyRangeMidiKeyboardComponent)
};


#endif /* UIControls_hpp */
