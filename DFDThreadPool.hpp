/******************************************************************
 This software has been developed 2017 by Rudi Leitner
 as open source under the GPL3 license.
 
 It uses the open source version of JUCE GRAPEFRUIT under GPL3.
 *******************************************************************/
//
//  DFDThreadPool.hpp
//  SimpleSamplerComponent
//
//  Created by Rudolf Leitner on 31/12/16.
//
//

#ifndef DFDThreadPool_hpp
#define DFDThreadPool_hpp

#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"
#include "SampleBin.hpp"







class SampleLoader : public ThreadPoolJob
{
public:
    SampleLoader(ThreadPool *pool_, SampleBinSample* aBinSample, DFDRingBuffer* aRingBuffer, int aRingBufferStartNdx, int aReadFromFrameNdx, int aNrOfFramesToRead);
    
    /** This fills the currently inactive buffer with samples from the SamplerSound.
     *
     *	The write buffer will be locked for the time of the read operation. Also it measures the time for getDiskUsage();
     */
    JobStatus runJob() override;
    
    // just a pointer to the used pool
    ThreadPool *backgroundPool;
    CriticalSection cs;
    
    //static double diskUsage;
    //static double lastCallToRequestData;
    //static void reset();
    //static double getDiskUsage();
private:
    DFDRingBuffer* ringBuffer;
    SampleBinSample* binSample;
    int ringBufferStartNdx;
    int readFromFrameNdx;
    int nrOfFramesToRead;
    
    
};


#endif /* DFDThreadPool_hpp */
