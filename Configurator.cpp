/******************************************************************
 This software has been developed 2017 by Rudi Leitner
 as open source under the GPL3 license.
 
 It uses the open source version of JUCE GRAPEFRUIT under GPL3.
 *******************************************************************/
//
//  PatchCreator.cpp
//  SimpleSampler
//
//  Created by Rudolf Leitner on 05/11/16.
//
//

#include "Configurator.hpp"



Configurator::Configurator()
{
}






bool Configurator::configureFromXMLFile(  std::vector<PatchDefinition*>& patchDefinitions, UIElementConfigs& uiConfigurations,std::string aPath )
{
    
    if( aPath == "")
        return false;
    
    File file( aPath );
    FileInputStream inStream( file );
    
    if( inStream.failedToOpen() || inStream.isExhausted())
        return false;
    
    
    std::string xml = inStream.readEntireStreamAsString().toStdString();
    
    return configureFromXML(patchDefinitions, uiConfigurations, xml);
}



bool Configurator::configureFromXML(  std::vector<PatchDefinition*>& patchDefinitions, UIElementConfigs& uiConfigurations,std::string aXML )
{
    ScopedPointer<XmlElement> xmlDoc = XmlDocument::parse( aXML );
    //Logger::getCurrentLogger()->writeToLog("createPatchesFromXML");
    
    if( xmlDoc->hasTagName( "Instrument" )) {
        //Logger::getCurrentLogger()->writeToLog("Top Node " + xmlDoc->getTagName());
        
        XmlElement* xmlEl = xmlDoc->getFirstChildElement();
        while( xmlEl != nullptr ) {
            //Logger::getCurrentLogger()->writeToLog("2nd Node " + xmlEl->getTagName());
            if( xmlEl->hasTagName("Patches")) {

                XmlElement* xmlPatch = xmlEl->getFirstChildElement();
                while( xmlPatch ) {
                    //Logger::getCurrentLogger()->writeToLog("3rd Node " + xmlPatch->getTagName());
                    if( xmlPatch->hasTagName("Patch")) {
                        PatchDefinition* aPatchDefinition = createPatchFromXML( xmlPatch );
                        if( aPatchDefinition != NULL)
                            patchDefinitions.push_back(aPatchDefinition);
                    }
                    xmlPatch = xmlPatch->getNextElement();
                }
            } else if( xmlEl->hasTagName("UI") ) {
                uiConfigurations.w = xmlEl->getIntAttribute("w",640);
                uiConfigurations.h = xmlEl->getIntAttribute("h",400);

                XmlElement* xmlUiElement = xmlEl->getFirstChildElement();
                while( xmlUiElement ) {
                    //Logger::getCurrentLogger()->writeToLog("3rd Node " + xmlUiElement->getTagName());
                    if( xmlUiElement->hasTagName("Element")) {
                        UIElementConfig elementConfig = createElementConfigFromXML( xmlUiElement );
                        if( elementConfig.name != "") {
                            uiConfigurations.addElementConfig(elementConfig);
                        }
                    }
                    xmlUiElement = xmlUiElement->getNextElement();
                }
                
            }
            xmlEl = xmlEl->getNextElement();
        }
        return true;
    }

    return false;
}


UIElementConfig Configurator::createElementConfigFromXML( XmlElement* xmlUiElement )
{
    UIElementConfig uec;
    if( xmlUiElement->hasTagName("Element") ) {
        uec.name = xmlUiElement->getStringAttribute( "name","" ).toStdString();
        uec.x = xmlUiElement->getIntAttribute("x",-1);
        uec.y = xmlUiElement->getIntAttribute("y",-1);
        uec.w = xmlUiElement->getIntAttribute("w",-1);
        uec.h = xmlUiElement->getIntAttribute("h",-1);
        uec.sizeX = xmlUiElement->getIntAttribute("sizeX",-1);
        uec.sizeY = xmlUiElement->getIntAttribute("sizeY",-1);
        uec.param = xmlUiElement->getStringAttribute( "param","" ).toStdString();
        uec.visible = xmlUiElement->getBoolAttribute("visible",true);
    }
    return uec;

}



PatchDefinition* Configurator::createPatchFromXML( XmlElement* xmlPatch )
{
    if( !xmlPatch->hasTagName("Patch") )
        return nullptr;
    
    
    
    PatchDefinition* patchDef = new PatchDefinition( xmlPatch->getStringAttribute( "name" , "").toStdString());
    
    int displaceSemiTones = xmlPatch->getIntAttribute( "displaceSemiTones",0 );
    
    XmlElement* xmlSetup = xmlPatch->getChildByName("Setup");
    if( xmlSetup != nullptr) {
        patchDef->setNumberOfVoices( xmlSetup->getIntAttribute( "numVoices",8 ))	;
        patchDef->setGainDb( xmlSetup->getDoubleAttribute( "gainDb",0.0 ));
        patchDef->setPan( xmlSetup->getDoubleAttribute( "pan",0.0 ));
        patchDef->setTuneSt( xmlSetup->getIntAttribute( "tuneSt",0.0 ));
        patchDef->setTune( xmlSetup->getDoubleAttribute( "tune",0.0 ));
        patchDef->setVeloSens( xmlSetup->getDoubleAttribute( "veloSens",0.75 ));
        patchDef->setBevel( xmlSetup->getDoubleAttribute( "bevel",0.0 ));
        patchDef->setMonoMode( xmlSetup->getBoolAttribute( "monophonic", false));
    }
    
    XmlElement* xmlLayers = xmlPatch->getChildByName("Layers");
    if( xmlLayers != nullptr) {
        patchDef->setLayer1GainDb( xmlLayers->getDoubleAttribute( "gainLayer1",0.0 ));
        patchDef->setLayer2GainDb( xmlLayers->getDoubleAttribute( "gainLayer2",0.0 ));
        patchDef->setLayer3GainDb( xmlLayers->getDoubleAttribute( "gainLayer3",0.0 ));
        patchDef->setLayer4GainDb( xmlLayers->getDoubleAttribute( "gainLayer4",0.0 ));
    }
    
    XmlElement* xmlPressure = xmlPatch->getChildByName("Pressure");
    if( xmlPressure != nullptr) {
        PatchDefinition::PRESSUREMODE pressMode = PatchDefinition::PRESSUREMODE::VELOCITY;
        std::string pressModeStr = xmlPressure->getStringAttribute( "mode" ).toStdString();
        if( pressModeStr == "PRESSUREABS" ) {
            pressMode = PatchDefinition::PRESSUREMODE::PRESSUREABS;
        } else if (pressModeStr == "VELOCITYPRESSUREABS"){
            pressMode = PatchDefinition::PRESSUREMODE::VELOCITYPRESSUREABS;
        }
        patchDef->setPressureMode( pressMode, xmlPressure->getIntAttribute( "controller",0 ));
    }
    
    XmlElement* xmlPitchwheel = xmlPatch->getChildByName("Pitchwheel");
    if( xmlPitchwheel != nullptr) {
        PatchDefinition::PITCHWHEELMODE pitchMode = PatchDefinition::PITCHWHEELMODE::CONTINUOUS;
        std::string pitchModeStr = xmlPitchwheel->getStringAttribute( "mode" ).toStdString();
        if( pitchModeStr == "SEMITONES" ) {
            pitchMode = PatchDefinition::PITCHWHEELMODE::SEMITONES;
        } else if( pitchModeStr == "SEMITONES_RETRIGGER" ) {
            pitchMode = PatchDefinition::PITCHWHEELMODE::SEMITONES_RETRIGGER;
        }
        patchDef->setPitchWheelDefinition( pitchMode,
                                          xmlPitchwheel->getIntAttribute( "rangeSt",12 ),
                                          xmlPitchwheel->getDoubleAttribute("releaseTime",0.0),
                                          xmlPitchwheel->getDoubleAttribute("offsetTime",0.0),
                                          xmlPitchwheel->getDoubleAttribute("attackTime",0.0) );
    }
    
    XmlElement* xmlLegato = xmlPatch->getChildByName("Legato");
    if( xmlLegato != nullptr) {
    	PatchDefinition::LEGATOMODE legatoMode = PatchDefinition::LEGATOMODE::OFF;
        std::string pitchModeStr = xmlLegato->getStringAttribute( "mode" ).toStdString();
        if( pitchModeStr == "SIMPLE" ) {
            legatoMode = PatchDefinition::LEGATOMODE::SIMPLE;
        }
        patchDef->setLegatoDefinition( legatoMode,
                                      xmlLegato->getDoubleAttribute("releaseTime",0.0),
                                      xmlLegato->getDoubleAttribute("offsetTime",0.0),
                                      xmlLegato->getDoubleAttribute("attackTime",0.0) );
    }
    
    XmlElement* xmlDfd = xmlPatch->getChildByName("Dfd");
    if( xmlDfd != nullptr) {
        patchDef->setDfd( xmlDfd->getBoolAttribute("dfd",false),
                         xmlDfd->getIntAttribute("numRingBuffers",0),
                         xmlDfd->getIntAttribute("preloadFrames",44100),
                         xmlDfd->getIntAttribute("loadFrames",22050),
                         xmlDfd->getIntAttribute("loadAtFramesLeft",11025) );
    }
    
    XmlElement* xmlEnv = xmlPatch->getChildByName("AHDSREnvelope");
    if( xmlEnv != nullptr) {
        patchDef->setEnvelopeDefinition( xmlEnv->getDoubleAttribute("offsetTime",0.0),
                                        xmlEnv->getDoubleAttribute("attackTime",0.0),
                                        xmlEnv->getDoubleAttribute("holdTime",0.0),
                                        xmlEnv->getDoubleAttribute("decayTime",0.0),
                                        xmlEnv->getDoubleAttribute("sustainDb",0.0),
                                        xmlEnv->getDoubleAttribute("releaseTime",0.0) );
    }
    
    XmlElement* xmlKeyswitches = xmlPatch->getChildByName("Keyswitches");
    if( xmlKeyswitches != nullptr) {
        patchDef->setKeyswitchRange( xmlKeyswitches->getIntAttribute("rangeFromKey",-1) + displaceSemiTones,
                                    xmlKeyswitches->getIntAttribute("rangeToKey",-1) +displaceSemiTones );
        
        XmlElement* xmlKeyswitch = xmlKeyswitches->getFirstChildElement();
        while( xmlKeyswitch != nullptr ) {
            if( xmlKeyswitch->hasTagName("Keyswitch") ) {
                
                KeyswitchDefinition::KEYSWITCHTYPE aType = KeyswitchDefinition::KEYSWITCHTYPE::KEYSWITCHTYPE_SOUNDSET;
                std::string aTypeStr = xmlKeyswitch->getStringAttribute("type","SOUNDSET").toStdString();
                if( aTypeStr == "SIMPLELEGATO") {
                    aType = KeyswitchDefinition::KEYSWITCHTYPE::KEYSWITCHTYPE_SIMPLELEGATO;
                }
                
                patchDef->addKeyswitchDefinition( xmlKeyswitch->getStringAttribute("name","").toStdString(),
                                                 xmlKeyswitch->getIntAttribute("key",-1),
                                                 xmlKeyswitch->getBoolAttribute("permanent",false),
                                                 aType,
                                                 xmlKeyswitch->getIntAttribute("parameter",-1));
            }
            xmlKeyswitch = 	xmlKeyswitch->getNextElement();
        }
    }
    
    
    XmlElement* xmlMonoGroups = xmlPatch->getChildByName("MonoGroups");
    if( xmlMonoGroups != nullptr) {
        XmlElement* xmlMonoGroup = xmlMonoGroups->getFirstChildElement();
        while( xmlMonoGroup != nullptr ) {
            if( xmlMonoGroup->hasTagName("MonoGroup") ) {
                patchDef->addMonoGroupRange( xmlMonoGroup->getIntAttribute("rangeFromKey",-1),
                                            xmlMonoGroup->getIntAttribute("rangeToKey",-1));
            }
            xmlMonoGroup = 	xmlMonoGroup->getNextElement();
        }
    }

    XmlElement* xmlKeyGroups = xmlPatch->getChildByName("KeyGroups");
    if( xmlKeyGroups != nullptr) {
        XmlElement* xmlKeyGroup = xmlKeyGroups->getFirstChildElement();
        while( xmlKeyGroup != nullptr ) {
            if( xmlKeyGroup->hasTagName("KeyGroup") ) {
                patchDef->addKeyGroup( xmlKeyGroup->getBoolAttribute("mono",false),
                                      xmlKeyGroup->getIntAttribute("rangefromKey",0) + displaceSemiTones,
                                      xmlKeyGroup->getIntAttribute("rangeToKey",127) + displaceSemiTones,
                                      xmlKeyGroup->getDoubleAttribute("gainDb",0.0),
                                      xmlKeyGroup->getDoubleAttribute("pan",0.0),
                                      xmlKeyGroup->getIntAttribute("tuneSt",0),
                                      xmlKeyGroup->getDoubleAttribute("tuneCt",0.0));
            }
            xmlKeyGroup = 	xmlKeyGroup->getNextElement();
        }
    }

    
    XmlElement* xmlSounds = xmlPatch->getChildByName("Sounds");
    if( xmlSounds != nullptr) {
        patchDef->setPlayRange( xmlSounds->getIntAttribute("rangeFromKey",0) + displaceSemiTones,
                               xmlSounds->getIntAttribute("rangeToKey",127) + displaceSemiTones);
        patchDef->setBevelRange( xmlSounds->getIntAttribute("bevelFromKey",0) + displaceSemiTones,
                               xmlSounds->getIntAttribute("bevelToKey",127) + displaceSemiTones);
        
        
        XmlElement* xmlSound = xmlSounds->getFirstChildElement();
        while( xmlSound != nullptr ) {
            if( xmlSound->hasTagName("Sound") ) {
                SoundDefinition soundDef( xmlSound->getStringAttribute("name","").toStdString(),
                                         xmlSound->getIntAttribute("noteRangeFrom",-1)+displaceSemiTones,
                                         xmlSound->getIntAttribute("noteRangeTo",-1)+displaceSemiTones,
                                         xmlSound->getIntAttribute("centerNote",-1)+displaceSemiTones,
                                         xmlSound->getBoolAttribute("xFade",false),
                                         xmlSound->getDoubleAttribute("gainDb",0.0),
                                         xmlSound->getIntAttribute("tuneSt",0),
                                         xmlSound->getDoubleAttribute("tuneCt",0.0),
                                         xmlSound->getDoubleAttribute("pan",0.0),
                                         xmlSound->getIntAttribute("layer",-1),
                                         xmlSound->getIntAttribute("soundSet",-1) );
                
                XmlElement* xmlSoundElement = xmlSound->getFirstChildElement();
                while( xmlSoundElement != nullptr ) {
                    if( xmlSoundElement->hasTagName("SoundElement") ) {
                        
                        SoundElementDefinition selDef( xmlSoundElement->getDoubleAttribute("gainDb",0.0),
                                                      xmlSoundElement->getDoubleAttribute("velocityRangeFrom",0.0),
                                                      xmlSoundElement->getDoubleAttribute("velocityRangeTo",0.0),
                                                      xmlSoundElement->getDoubleAttribute("xFadeVelocityTo",0.0) );
                        
                        SoundElementDefinition::ROUNDROBINMODE aRRMode = SoundElementDefinition::ROUNDROBINMODE::OFF;
                        std::string aRRModeStr = xmlSoundElement->getStringAttribute("roundRobinMode","OFF").toStdString();
                        if( aRRModeStr == "CYCLE") {
                        	aRRMode = SoundElementDefinition::ROUNDROBINMODE::CYCLE;
                        } else if( aRRModeStr == "RANDOM") {
                        	aRRMode = SoundElementDefinition::ROUNDROBINMODE::RANDOM;
                        }
                        selDef.setRoundRobinMode( aRRMode );
                        
                        if( xmlSoundElement->hasAttribute("sample")) {
                            std::string aSampleName = xmlSoundElement->getStringAttribute("sample","").toStdString();
                            if( aSampleName != "" ) {
                                selDef.addRoundRobinSound( aSampleName );
                            }
                            
                        }
                        
                        XmlElement* xmlRRSample = xmlSoundElement->getFirstChildElement();
                        while( xmlRRSample != nullptr ) {
                            if( xmlRRSample->hasTagName("Sample") ) {
                                std::string aSampleName = xmlRRSample->getStringAttribute("name","").toStdString();
                                if( aSampleName != "" ) {
                                    selDef.addRoundRobinSound( aSampleName );
                                }	
                            }
                        }
                        soundDef.addSoundElementDefinition( selDef );
                    }
                    
                    xmlSoundElement = xmlSoundElement->getNextElement();
                }
                
                patchDef->addSoundDefinition( soundDef );	
            }
            xmlSound = xmlSound->getNextElement();
        }
    }
    return patchDef;	
}




























