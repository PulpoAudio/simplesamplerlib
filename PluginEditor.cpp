/******************************************************************
 This software has been developed 2017 by Rudi Leitner
 as open source under the GPL3 license.
 
 It uses the open source version of JUCE GRAPEFRUIT under GPL3.
 *******************************************************************/
/*
  ==============================================================================

    This file was auto-generated!

    It contains the basic framework code for a JUCE plugin editor.

  ==============================================================================
*/

#include "PluginProcessor.h"
#include "PluginEditor.h"
#include "UIImages.h"
#include "BackGround.h"



//==============================================================================
// This is a handy slider subclass that controls an AudioProcessorParameter
// (may move this class into the library itself at some point in the future..)



//==============================================================================
SimpleSamplerComponentAudioProcessorEditor::SimpleSamplerComponentAudioProcessorEditor (SimpleSamplerComponentAudioProcessor& p, UIElementConfigs* aUIConfigurations)
    : AudioProcessorEditor (&p), processor (p),
    gainLabel (String(), "Gain"),
    panLabel(String(), "Pan"),
    tuneStLabel(String(), "TuneSt"),
    tuneLabel(String(), "Tune"),
    veloSensLabel(String(), "Vel Sense"),
    bevelLabel(String(),"Bevel"),
    offsetTLabel (String(), "Offset"),
    attackTLabel (String(), "Attack"),
    holdTLabel (String(), "Hold"),
    decayTLabel (String(), "Decay"),
    sustainDbLabel (String(), "Sustain"),
    releaseTLabel (String(), "Release"),
    layer1Label(String(), "Layer1"),
    layer2Label(String(), "Layer2"),
    layer3Label(String(), "Layer3"),
    layer4Label(String(), "Layer4"),
    pressureModeLabel(String(), "Pressure Mode"),
    pressureControllerLabel(String(), "Press. Controller"),
    pitchWheelModeLabel(String(), "Pitchwheel Mode"),
    pitchWheelRangeLabel(String(), "Range"),
    pitchWheelReleaseTLabel(String(), "Release"),
    pitchWheelOffsetTLabel(String(), "Offset"),
    pitchWheelAttackTLabel(String(), "Attack"),
    legatoModeLabel(String(), "Legato Mode"),
    legatoReleaseTLabel(String(), "Release"),
    legatoOffsetTLabel(String(), "Offset"),
    legatoAttackTLabel(String(), "Attack"),
    keySwitchRangeShiftlabel(String(), "KS Range+/-"),
    playRangeShiftLabel(String(), "Play Range+/-"),
	monoModeLabel(String(), "Monophonic"),
    dfdLabel(String(),"DFD"),
    soundSetLabel(String(), "SoundSet")
{
    patchDefinition = NULL;
    uiConfigurations = aUIConfigurations;

    backgroundImg = ImageCache::getFromMemory(BackGround::BackGround_png, BackGround::BackGround_pngSize);
    rotarySliderKnobManImage =  ImageCache::getFromMemory(UIImages::knob_red_png, UIImages::knob_red_pngSize);
    toggleButtonKnobManImage =  ImageCache::getFromMemory(UIImages::toggle_red_png, UIImages::toggle_red_pngSize);
    lookAndFeel.setRotarySliderKnobManImage(&rotarySliderKnobManImage,32,32,101);
    lookAndFeel.setToggleButtonKnobManImage(&toggleButtonKnobManImage,32,32,2);

    
    
    gainLabel.setJustificationType(juce::Justification::centred);
    panLabel.setJustificationType(juce::Justification::centred);
    tuneStLabel.setJustificationType(juce::Justification::centred);
    tuneLabel.setJustificationType(juce::Justification::centred);
    veloSensLabel.setJustificationType(juce::Justification::centred);
    bevelLabel.setJustificationType(juce::Justification::centred);
    offsetTLabel.setJustificationType(juce::Justification::centred);
    attackTLabel.setJustificationType(juce::Justification::centred);
    holdTLabel.setJustificationType(juce::Justification::centred);
    decayTLabel.setJustificationType(juce::Justification::centred);
    sustainDbLabel.setJustificationType(juce::Justification::centred);
    releaseTLabel.setJustificationType(juce::Justification::centred);
    layer1Label.setJustificationType(juce::Justification::centred);
    layer2Label.setJustificationType(juce::Justification::centred);
    layer3Label.setJustificationType(juce::Justification::centred);
    layer4Label.setJustificationType(juce::Justification::centred);
    pressureModeLabel.setJustificationType(juce::Justification::centred);
    pressureControllerLabel.setJustificationType(juce::Justification::centred);
    pitchWheelModeLabel.setJustificationType(juce::Justification::centred);
    pitchWheelRangeLabel.setJustificationType(juce::Justification::centred);
    pitchWheelReleaseTLabel.setJustificationType(juce::Justification::centred);
    pitchWheelOffsetTLabel.setJustificationType(juce::Justification::centred);
    pitchWheelAttackTLabel.setJustificationType(juce::Justification::centred);
    legatoModeLabel.setJustificationType(juce::Justification::centred);
    legatoReleaseTLabel.setJustificationType(juce::Justification::centred);
    legatoOffsetTLabel.setJustificationType(juce::Justification::centred);
    legatoAttackTLabel.setJustificationType(juce::Justification::centred);
    keySwitchRangeShiftlabel.setJustificationType(juce::Justification::centred);
    playRangeShiftLabel.setJustificationType(juce::Justification::centred);
    monoModeLabel.setJustificationType(juce::Justification::centred);
    soundSetLabel.setJustificationType(juce::Justification::centred);
    dfdLabel.setJustificationType(juce::Justification::centred);
    


    
    addLabel( gainLabel, "LbGain");
    addLabel( panLabel, "LbPan");
    addLabel( tuneStLabel, "LbTuneSt");
    addLabel( tuneLabel, "LbTune");
    addLabel( veloSensLabel, "LbVelSens");
    addLabel( bevelLabel, "LbBevel");
    addLabel(offsetTLabel,"LbEndOffset");
    addLabel(attackTLabel,"LbEnvAttack");
    addLabel(holdTLabel,"LbEnvHold");
    addLabel(decayTLabel,"LbEnvDecay");
    addLabel(sustainDbLabel,"LbEnvSustain");
    addLabel(releaseTLabel,"LbEnvRelease");
    addLabel(layer1Label,"LbLayer1Gain");
    addLabel(layer2Label,"LbLayer2Gain");
    addLabel(layer3Label,"LbLayer3Gain");
    addLabel(layer4Label,"LbLayer4Gain");
    addLabel(pressureModeLabel,"LbPressureMode");
    addLabel(pressureControllerLabel,"LbPressureController");
    addLabel(pitchWheelModeLabel,"LbPitchWeelMode");
    addLabel(pitchWheelRangeLabel,"LbPitchWheelRange");
    addLabel(pitchWheelReleaseTLabel,"LbPitchWheelRelease");
    addLabel(pitchWheelOffsetTLabel,"LbPitchWheelOffset");
    addLabel(pitchWheelAttackTLabel,"LbPitchWheelAttack");
    addLabel(legatoModeLabel,"LbLegatoMode");
    addLabel(legatoReleaseTLabel,"LbLegatoRelease");
    addLabel(legatoOffsetTLabel,"LbLegatoOffset");
    addLabel(legatoAttackTLabel,"LbLegatoAttack");
    addLabel(keySwitchRangeShiftlabel,"LbKeySwitchRangeShift");
    addLabel(playRangeShiftLabel,"LbPlayRangeShift");
    addLabel(monoModeLabel,"LbMonoMode");
    addLabel(soundSetLabel,"LbSoundSet");
    addLabel(dfdLabel,"LbDfd");
    
    addChildComponent(reloadButton = new TextButton("reload"));
    reloadButton->addListener(this);
    
    // ===============
    midiKeyboard = new ParameterKeyRangeMidiKeyboardComponent(&uiUpdateTimer, p.keyboardState, MidiKeyboardComponent::horizontalKeyboard,*p.getPluginParameters()->playRangeShiftParam, *p.getPluginParameters()->keySwitchRangeShiftParam);
    addComponent( *midiKeyboard,"CoMidiKeyboard");
    // ===============
    keySwitchRangeShiftSlider= new ParameterSlider (&lookAndFeel, &uiUpdateTimer, *p.getPluginParameters()->keySwitchRangeShiftParam, " st",1.0f);
    addSlider (*keySwitchRangeShiftSlider, "SlPlayRangeShift");
    
    playRangeShiftSlider= new ParameterSlider (&lookAndFeel, &uiUpdateTimer, *p.getPluginParameters()->playRangeShiftParam, " st",1.0f);
    addSlider (*playRangeShiftSlider, "SlKeySwitchRangeShift");
    monoModeToggleButton = new ParameterToggleButton(&lookAndFeel, &uiUpdateTimer, *p.getPluginParameters()->monoModeParam);
    addButton( *monoModeToggleButton, "BuMonoMode");
    soundSetCombo = new ParameterIntComboBox (&lookAndFeel, &uiUpdateTimer, *p.getPluginParameters()->soundSetParam);
    addComboBox(*soundSetCombo, "CbSoundSet");
    dfdToggleButton = new ParameterToggleButton(&lookAndFeel, &uiUpdateTimer, *p.getPluginParameters()->dfdParam);
    addButton( *dfdToggleButton,"BuDfd");
    // ===============
    gainSlider = new DBParameterSlider (&lookAndFeel, &uiUpdateTimer, *p.getPluginParameters()->gainParam, 0.25f);
    addSlider (*gainSlider, "SlGain");
    panSlider = new PrcParameterSlider (&lookAndFeel, &uiUpdateTimer, *p.getPluginParameters()->panParam, 1.0f);
    addSlider (*panSlider, "SlPan");
    tuneStSlider = new ParameterSlider (&lookAndFeel, &uiUpdateTimer, *p.getPluginParameters()->tuneStParam, " st", 1.0f);
    addSlider (*tuneStSlider, "SlTuneSt");
    tuneSlider = new ParameterSlider (&lookAndFeel, &uiUpdateTimer, *p.getPluginParameters()->tuneParam, " st", 1.0f);
    addSlider (*tuneSlider, "SlTune");
    veloSensSlider = new ParameterSlider (&lookAndFeel, &uiUpdateTimer, *p.getPluginParameters()->veloSensParam, "", 1.0f);
    addSlider (*veloSensSlider, "SlVelSens");
    bevelSlider = new ParameterSlider (&lookAndFeel, &uiUpdateTimer, *p.getPluginParameters()->bevelParam, "", 1.0f);
    addSlider (*bevelSlider, "SlBevel");
    // ===============
    pressureModeCombo = new ParameterChoiceComboBox (&lookAndFeel, &uiUpdateTimer, *p.getPluginParameters()->pressureModeParam);
    pressureControllerCombo = new ParameterChoiceComboBox (&lookAndFeel, &uiUpdateTimer, *p.getPluginParameters()->pressureControllerParam);
    addComboBox( *pressureModeCombo,"CbPressureMode");
    addComboBox( *pressureControllerCombo,"CbPressureController");
    // ===============
    layer1GainSlider = new DBParameterSlider (&lookAndFeel, &uiUpdateTimer, *p.getPluginParameters()->layerGainParams[0], 0.25f);
    layer2GainSlider = new DBParameterSlider (&lookAndFeel, &uiUpdateTimer, *p.getPluginParameters()->layerGainParams[1], 0.25f);
    layer3GainSlider = new DBParameterSlider (&lookAndFeel, &uiUpdateTimer, *p.getPluginParameters()->layerGainParams[2], 0.25f);
    layer4GainSlider = new DBParameterSlider (&lookAndFeel, &uiUpdateTimer, *p.getPluginParameters()->layerGainParams[3], 0.25f);
    addSlider (*layer1GainSlider, "SlLayer1Gain");
    addSlider (*layer2GainSlider, "SlLayer2Gain");
    addSlider (*layer3GainSlider, "SlLayer3Gain");
    addSlider (*layer4GainSlider, "SlLayer4Gain");
    // ===============
    offsetTSlider = new ParameterSlider (&lookAndFeel, &uiUpdateTimer, *p.getPluginParameters()->offsetTParam," s", 0.25f);
    attackTSlider = new ParameterSlider (&lookAndFeel, &uiUpdateTimer, *p.getPluginParameters()->attackTParam," s", 0.25f);
    holdTSlider = new ParameterSlider (&lookAndFeel, &uiUpdateTimer, *p.getPluginParameters()->holdTParam, " s", 0.25f);
    decayTSlider = new ParameterSlider (&lookAndFeel, &uiUpdateTimer, *p.getPluginParameters()->decayTParam, " s", 0.25f);
    sustainDbSlider = new DBParameterSlider (&lookAndFeel, &uiUpdateTimer, *p.getPluginParameters()->sustainGainParam, 0.25f);
    releaseTSlider = new ParameterSlider (&lookAndFeel, &uiUpdateTimer, *p.getPluginParameters()->releaseTParam, " s", 0.25f);
    addSlider (*offsetTSlider, "SlEnvOffset");
    addSlider (*attackTSlider, "SlEnvAttack");
    addSlider (*holdTSlider, "SlEnvHold");
    addSlider (*decayTSlider, "SlEnvDecay");
    addSlider (*sustainDbSlider, "SlEnvSustain");
    addSlider (*releaseTSlider, "SlEnvRelease");
    // ===============
    pitchWheelModeCombo = new ParameterChoiceComboBox (&lookAndFeel, &uiUpdateTimer, *p.getPluginParameters()->pitchWheelModeParam);
    pitchWheelRangeSlider = new ParameterSlider (&lookAndFeel, &uiUpdateTimer, *p.getPluginParameters()->pitchWheelRangeParam, " st", 1.0f);
    pitchWheelRetriggerReleaseTSlider= new ParameterSlider (&lookAndFeel, &uiUpdateTimer, *p.getPluginParameters()->pitchWheelRetriggerReleaseTParam, " s", 0.25f);
    pitchWheelRetriggerOffsetTSlider= new ParameterSlider (&lookAndFeel, &uiUpdateTimer, *p.getPluginParameters()->pitchWheelRetriggerOffsetTParam, " s", 0.25f);
    pitchWheelRetriggerAttackTSlider= new ParameterSlider (&lookAndFeel, &uiUpdateTimer, *p.getPluginParameters()->pitchWheelRetriggerAttackTParam, " s", 0.25f);
    addComboBox( *pitchWheelModeCombo,"CbPitchWheelMode");
    addSlider( *pitchWheelRangeSlider,"SlPitchWheelRange");
    addSlider( *pitchWheelRetriggerReleaseTSlider,"SlPitchWheelRelease");
    addSlider( *pitchWheelRetriggerOffsetTSlider,"SlPitchWheelOffset");
    addSlider( *pitchWheelRetriggerAttackTSlider,"SlPitchWheelAttack");
    // ===============
    legatoModeCombo= new ParameterChoiceComboBox (&lookAndFeel, &uiUpdateTimer, *p.getPluginParameters()->legatoModeParam);
    legatoReleaseTSlider= new ParameterSlider (&lookAndFeel, &uiUpdateTimer, *p.getPluginParameters()->legatoReleaseTParam, " s", 0.25f);
    legatoOffsetTSlider= new ParameterSlider (&lookAndFeel, &uiUpdateTimer, *p.getPluginParameters()->legatoOffsetTParam, " s", 0.25f);
    legatoAttackTSlider= new ParameterSlider (&lookAndFeel, &uiUpdateTimer, *p.getPluginParameters()->legatoAttackTParam, " s", 0.25f);
    addComboBox( *legatoModeCombo,"CbLegatoMode");
    addSlider( *legatoReleaseTSlider,"SlLegatoRelease");
    addSlider( *legatoOffsetTSlider,"SlLegatoOffset");
    addSlider( *legatoAttackTSlider,"SLLegatoAttack");
    // ===============
    
    
    // add the midi keyboard component..
    //addChildComponent (midiKeyboard);
    
    
    // Make sure that before the constructor has finished, you've set the
    // editor's size to whatever you need it to be.
    setSize (aUIConfigurations->w, aUIConfigurations->h);
    configureComponents();
    
    
    
    licInfo =  "DRY AGED       by pulpoaudio            www.pulponet.com\n\n";
    licInfo += "Developed 2017 by Rudi Leitner\n\n";
    licInfo += "This AU component uses the JUCE Grapefruit library.\n";
    licInfo += "JUCE is available under different liceses. This component\n";
    licInfo += "uses the Open Source version of JUCE under GPL 3\n\n";
    licInfo += "Go to https://bitbucket.org/DukeRoodee/dryaged.git  for source codes.\n";
    licInfo += "Follow the instructions in the README file of the project.\n";
    
    addChildComponent(aboutButton = new TextButton("about"));
    aboutButton->setLookAndFeel(&lookAndFeel);
    aboutButton->setVisible(true);
    aboutButton->setBounds(aUIConfigurations->w-70,0,70,20);
    aboutButton->addListener(this);
    
    
    
    
    
}

SimpleSamplerComponentAudioProcessorEditor::~SimpleSamplerComponentAudioProcessorEditor()
{
}

//==============================================================================
void SimpleSamplerComponentAudioProcessorEditor::paint (Graphics& g)
{

    
    //g.setGradientFill (ColourGradient (Colours::white, 0, 0,
    //                                   Colours::lightgrey, 0, (float) getHeight(), false));
    //g.fillAll();
    
    //g.setColour(juce::Colour::Colour(0,0,0));
    //g.drawRect(5,30,300,100);
    
    g.drawImage( backgroundImg,0,0,backgroundImg.getWidth(),backgroundImg.getHeight(),0,0,backgroundImg.getWidth(),backgroundImg.getHeight());
}


void SimpleSamplerComponentAudioProcessorEditor::addLabel( Label& aLabel, std::string aName)
{
    UIElementConfig* uec = uiConfigurations->getElementConfig(aName);
    if( uec != NULL && uec->param != "" )
        aLabel.setText(uec->param, NotificationType::dontSendNotification );
    addChildComponent(aLabel);
}

void SimpleSamplerComponentAudioProcessorEditor::configureLabel( Label& aLabel, std::string aName)
{
    UIElementConfig* uec = uiConfigurations->getElementConfig(aName);
    if( uec != NULL ) {
        if( uec->hasBounds()) {
            aLabel.setBounds( uec->x, uec->y, uec->w, uec->h);
        }
        aLabel.setVisible(uec->visible);
    }
}

void SimpleSamplerComponentAudioProcessorEditor::addSlider( Slider& aSlider, std::string aName)
{
    //UIElementConfig* uec = uiConfigurations->getElementConfig(aName);
    //if( uec != NULL && uec->param != "" )
    //    aSlider.setText(uec->param, NotificationType::dontSendNotification );
    addChildComponent(aSlider);
    
}

void SimpleSamplerComponentAudioProcessorEditor::configureSlider( Slider& aSlider, std::string aName)
{
    UIElementConfig* uec = uiConfigurations->getElementConfig(aName);
    if( uec != NULL ) {
        if( uec->hasBounds()) {
            aSlider.setBounds( uec->x, uec->y, uec->w, uec->h);
        }
        aSlider.setVisible(uec->visible);
    }
}

void SimpleSamplerComponentAudioProcessorEditor::addComboBox( ComboBox& aCombo, std::string aName)
{
    //UIElementConfig* uec = uiConfigurations->getElementConfig(aName);
    //if( uec != NULL && uec->param != "" )
    //    aSlider.setText(uec->param, NotificationType::dontSendNotification );
    addChildComponent(aCombo);
    
}

void SimpleSamplerComponentAudioProcessorEditor::configureComboBox( ComboBox& aCombo, std::string aName)
{
    UIElementConfig* uec = uiConfigurations->getElementConfig(aName);
    if( uec != NULL ) {
        if( uec->hasBounds()) {
            aCombo.setBounds( uec->x, uec->y, uec->w, uec->h);
        }
        if( uec->hasSize()) {
            aCombo.setSize(uec->sizeX, uec->sizeY);
        }
        aCombo.setVisible(uec->visible);
    }
}

void SimpleSamplerComponentAudioProcessorEditor::addButton( Button& aButton, std::string aName)
{
    //UIElementConfig* uec = uiConfigurations->getElementConfig(aName);
    //if( uec != NULL && uec->param != "" )
    //    aButton.setText(uec->param, NotificationType::dontSendNotification );
    addChildComponent(aButton);

}
void SimpleSamplerComponentAudioProcessorEditor::configureButton( Button& aButton, std::string aName)
{
    UIElementConfig* uec = uiConfigurations->getElementConfig(aName);
    if( uec != NULL ) {
        if( uec->hasBounds()) {
            aButton.setBounds( uec->x, uec->y, uec->w, uec->h);
        }
        aButton.setVisible(uec->visible);
    }
}


void SimpleSamplerComponentAudioProcessorEditor::addComponent( Component& aComponent, std::string aName)
{
    //UIElementConfig* uec = uiConfigurations->getElementConfig(aName);
    //if( uec != NULL && uec->param != "" )
    //    aButton.setText(uec->param, NotificationType::dontSendNotification );
    addChildComponent(aComponent);
    
}

void SimpleSamplerComponentAudioProcessorEditor::configureComponent( Component& aComponent, std::string aName)
{
    UIElementConfig* uec = uiConfigurations->getElementConfig(aName);
    if( uec != NULL ) {
        if( uec->hasBounds()) {
            aComponent.setBounds( uec->x, uec->y, uec->w, uec->h);
        }
        if( uec->hasSize()) {
            aComponent.setSize(uec->sizeX, uec->sizeY);
        }
        aComponent.setVisible(uec->visible);
    }
}


void SimpleSamplerComponentAudioProcessorEditor::configureComponents()
{
    // This lays out our child components...
    
#ifdef SHOW_RELOAD_BUTTON
    reloadButton->setBounds(0,0,70,20);
#else
    reloadButton->setVisible(false);
#endif
    
    
    /*Rectangle<int> r (getLocalBounds().reduced (8));
     
     midiKeyboard->setBounds (r.removeFromBottom (70));
     midiKeyboard->setVisible(true);
     r.removeFromTop (10);
     */
    
    configureComponent( *midiKeyboard, "CoMidiKeyboard");
    
    
    configureLabel( playRangeShiftLabel, "LbPlayRangeShift");
    configureLabel( keySwitchRangeShiftlabel, "LbKeySwitchRangeShift");
    configureLabel( monoModeLabel, "LbMonoMode");
    configureLabel( soundSetLabel, "LbSoundSet");
    configureLabel( dfdLabel, "LbDfd");
    
    configureSlider( *playRangeShiftSlider,"SlPlayRangeShift");
    configureSlider( *keySwitchRangeShiftSlider,"SlKeySwitchRangeShift");
    configureButton( *monoModeToggleButton, "BuMonoMode");
    configureButton( *dfdToggleButton, "BuDfd");
    configureComboBox(*soundSetCombo, "CbSoundSet");
    
    
    configureLabel( gainLabel, "LbGain");
    configureLabel( panLabel, "LbPan");
    configureLabel( tuneLabel, "LbTune");
    configureLabel( tuneStLabel, "LbTuneSt");
    configureLabel( veloSensLabel, "LbVelSens");
    
    configureSlider( *gainSlider,"SlGain");
    configureSlider( *panSlider,"SlPan");
    configureSlider( *tuneSlider,"SlTune");
    configureSlider( *tuneStSlider,"SlTuneSt");
    configureSlider( *veloSensSlider,"SlVelSens");
    configureSlider( *bevelSlider,"SlBevel");
    
    
    configureLabel( pressureModeLabel, "LbPressureMode");
    configureLabel( pressureControllerLabel, "LbPressureController");
    
    configureComboBox(*pressureModeCombo, "CbPressureMode");
    configureComboBox(*pressureControllerCombo, "CbPressureController");
    
    
    
    configureLabel( layer1Label, "LbLayer1Gain");
    configureLabel( layer2Label, "LbLayer2Gain");
    configureLabel( layer3Label, "LbLayer3Gain");
    configureLabel( layer4Label, "LbLayer4Gain");
    
    configureSlider( *layer1GainSlider,"SlLayer1Gain");
    configureSlider( *layer2GainSlider,"SlLayer2Gain");
    configureSlider( *layer3GainSlider,"SlLayer3Gain");
    configureSlider( *layer4GainSlider,"SlLayer4Gain");
    
    
    
    configureLabel( offsetTLabel, "LbEnvOffset");
    configureLabel( attackTLabel, "LbEnvAttack");
    configureLabel( holdTLabel, "LbEnvHold");
    configureLabel( decayTLabel, "LbEnvDecay");
    configureLabel( sustainDbLabel, "LbEnvSustain");
    configureLabel( releaseTLabel, "LbEnvRelease");
    
    configureSlider( *offsetTSlider ,"SlEnvOffset");
    configureSlider( *attackTSlider ,"SlEnvAttack");
    configureSlider( *holdTSlider ,"SlEnvHold");
    configureSlider( *decayTSlider ,"SlEnvDecay");
    configureSlider( *sustainDbSlider ,"SlEnvSustain");
    configureSlider( *releaseTSlider ,"SlEnvRelease");
    
    
    
    configureLabel( legatoModeLabel, "LbLegatoMode");
    configureLabel( legatoReleaseTLabel, "LbLegatoRelease");
    configureLabel( legatoOffsetTLabel, "LbLegatoOffset");
    configureLabel( legatoAttackTLabel, "LbLegatoAttack");
    
    configureComboBox(*legatoModeCombo, "CbLegatoMode");
    configureSlider(*legatoReleaseTSlider, "SlLegatoRelease");
    configureSlider(*legatoOffsetTSlider, "SlLegatoOffset");
    configureSlider(*legatoAttackTSlider, "SlLegatoAttack");
    
    
    configureLabel( pitchWheelModeLabel, "LbPitchWheelMode");
    configureLabel( pitchWheelRangeLabel, "LbPitchWheelRange");
    configureLabel( pitchWheelReleaseTLabel, "LbPitchWheelRelease");
    configureLabel( pitchWheelOffsetTLabel, "LbPitchWheelOffset");
    configureLabel( pitchWheelAttackTLabel, "LbPitchWheelAttack");
    
    configureComboBox(*pitchWheelModeCombo , "CbPitchWheelMode");
    configureSlider(*pitchWheelRangeSlider ,"SlPitchWheelRange");
    configureSlider(*pitchWheelRetriggerReleaseTSlider ,"SlPitchWheelRelease");
    configureSlider(*pitchWheelRetriggerOffsetTSlider ,"SlPitchWheelOffset");
    configureSlider(*pitchWheelRetriggerAttackTSlider ,"SlPitchWheelAttack");
    
    
}



void SimpleSamplerComponentAudioProcessorEditor::resized()
{
}



void SimpleSamplerComponentAudioProcessorEditor::setPatchDefinition(PatchDefinition* aPatchDefinition)
{
	patchDefinition = aPatchDefinition;
	if( patchDefinition != NULL) {
	    midiKeyboard->setPlayRange( patchDefinition->playRangeFromMidiNote, patchDefinition->playRangeToMidiNote);
	    midiKeyboard->setKeyswitchRange( patchDefinition->keySwitchRangeFromMidiNote, patchDefinition->keySwitchRangeToMidiNote);
	    midiKeyboard->setPlayRangeShift( 0 );
	    midiKeyboard->setKeyswitchRangeShift(0);
        midiKeyboard->centerVisibleRange();

	    StringArray& soundSetNames = patchDefinition->getSoundSetNames();
	    soundSetCombo->setSelectionTexts( soundSetNames );
	}
}




void SimpleSamplerComponentAudioProcessorEditor::buttonClicked (Button* buttonThatWasClicked)
{
	if( buttonThatWasClicked == reloadButton) {
        
		((SimpleSamplerComponentAudioProcessor*)this->getAudioProcessor())->reload();
    } else if( buttonThatWasClicked == aboutButton) {
        AlertWindow::showMessageBoxAsync(AlertWindow::AlertIconType::InfoIcon, "About DRY AGED", licInfo, "OK", NULL,NULL);
    }
    

}
