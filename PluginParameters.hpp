/******************************************************************
 This software has been developed 2017 by Rudi Leitner
 as open source under the GPL3 license.
 
 It uses the open source version of JUCE GRAPEFRUIT under GPL3.
 *******************************************************************/
//
//  PluginParameters.hpp
//  SimpleSamplerComponent
//
//  Created by Rudolf Leitner on 27/11/16.
//
//

#ifndef PluginParameters_hpp
#define PluginParameters_hpp

#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"



/*--------------------------------------------------------------------------------------------------------------
 * PluginParameters
 *
 * ADAPTER for all Plugin Parameters
 * Although we can somehow access theses parameters by  processor::getParameter( ndx),
 * we dont want this because some parameters like gain are used for calculation of each seperate sample and
 * may respond immediately.  So this collection of pointers will use less CPU.
 ---------------------------------------------------------------------------------------------------------------*/

class PluginParameters {
    
public:

    /*enum ParamNumber {    not use at the moment
        GAIN = 0,
        PAN,
        TUNE,
        VELOSENS,
        OFFSETT,
        ATTACKT,
        HOLDT,
        DECAYT,
        SUSTAINDB,
        RELEASET,
        LAYER1GAIN,
        LAYER2GAIN,
        LAYER3GAIN,
        LAYER4GAIN,
        PRESSUREMODE,
        PRESSURECONTROLLER,
        PWMODE,
        PWRANGE,
        PWRELEASET,
        PWOFFSETT,
        PWATTACKT,
        LEGMODE,
        LEGRELEASET,
        LEGOFFSETT,
        LEGATTACKT
    };*/
    
    AudioParameterFloat* gainParam;
    AudioParameterFloat* panParam;
    AudioParameterInt* tuneStParam;
    AudioParameterFloat* tuneParam;
    AudioParameterFloat* veloSensParam;
    AudioParameterFloat* bevelParam;

    AudioParameterInt* keySwitchRangeShiftParam;
    AudioParameterInt* playRangeShiftParam;
    
    AudioParameterFloat* offsetTParam;
    AudioParameterFloat* attackTParam;
    AudioParameterFloat* holdTParam;
    AudioParameterFloat* decayTParam;
    AudioParameterFloat* sustainGainParam;
    AudioParameterFloat* releaseTParam;
    
    AudioParameterFloat* layerGainParams[4];
    
    AudioParameterChoice* pressureModeParam;
    AudioParameterChoice* pressureControllerParam;
    
    AudioParameterChoice* pitchWheelModeParam;
    AudioParameterInt* pitchWheelRangeParam;
    AudioParameterFloat* pitchWheelRetriggerReleaseTParam;
    AudioParameterFloat* pitchWheelRetriggerOffsetTParam;
    AudioParameterFloat* pitchWheelRetriggerAttackTParam;
    
    
    AudioParameterChoice* legatoModeParam;
    AudioParameterFloat* legatoReleaseTParam;
    AudioParameterFloat* legatoOffsetTParam;
    AudioParameterFloat* legatoAttackTParam;
    
    AudioParameterBool*  monoModeParam;

    AudioParameterInt* soundSetParam;


    AudioParameterBool* dfdParam;
    AudioParameterInt* dfdPreloadFrames;
    AudioParameterInt* dfdLoadFrames;
    AudioParameterInt* dfdLoadAtFramesLeft;
    //bool dfdParam;
    //float dfdPreloadFrames;
    //float dfdLoadFrames;
    //float dfdPreloadTimeParam;

    
    PluginParameters();
};




#endif /* PluginParameters_hpp */
