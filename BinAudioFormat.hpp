/******************************************************************
 This software has been developed 2017 by Rudi Leitner
 as open source under the GPL3 license.
 
 It uses the open source version of JUCE GRAPEFRUIT under GPL3.
 *******************************************************************/
//
//  BinAudioFormat.hpp
//  SimpleSamplerComponent
//
//  Created by Rudolf Leitner on 26/12/16.
//
//

#ifndef BinAudioFormat_hpp 
#define BinAudioFormat_hpp

#include <stdio.h>
#include "JuceHeader.h"
#include "SampleBin.hpp"




/*--------------------------------------------------------------------------------------------------------------
 * BinAudioFormat
 *
 * Audio Format, FormatReader and MemoryMappedReader for a BIN file.
 * A BIN file carries a block with information for each sample  (samplerate, channels, startbyte, length, etc)
 * and afterwards the sample frames for all samples.
 *
 ---------------------------------------------------------------------------------------------------------------*/

class  BinAudioFormat  : public AudioFormat
{
public:
    //==============================================================================
    /** Creates a format object. */
    BinAudioFormat(SampleBinSample* aBinSample);
    
    /** Destructor. */
    ~BinAudioFormat();
    
    
    //==============================================================================
    Array<int> getPossibleSampleRates() override;
    Array<int> getPossibleBitDepths() override;
    bool canDoStereo() override;
    bool canDoMono() override;
    
    //==============================================================================
    AudioFormatReader* createReaderFor (InputStream* sourceStream,
                                        bool deleteStreamIfOpeningFails) override;
    
    MemoryMappedAudioFormatReader* createMemoryMappedReader (const File&) override;
    
    AudioFormatWriter* createWriterFor (OutputStream* streamToWriteTo,
                                        double sampleRateToUse,
                                        unsigned int numberOfChannels,
                                        int bitsPerSample,
                                        const StringPairArray& metadataValues,
                                        int qualityOptionIndex) override;
    
    
private:
    SampleBinSample* binSample;

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (BinAudioFormat)
};



/*--------------------------------------------------------------------------------------------------------------
 * BinAudioFormatReader
 *
 *
 *
 ---------------------------------------------------------------------------------------------------------------*/

class BinAudioFormatReader  : public AudioFormatReader
{
private:
    SampleBinSample* binSample;
    
    //Dont use the AudioFormatReader::input as input stream because this is getting deleted int the destructor
    //of AudioFormatReader.    As the input stream is one and the same for all BinAudioFormatReader objects,
    //we dont want to delete it !
    InputStream* binInputStream;

public:
    BinAudioFormatReader (InputStream* const in, SampleBinSample* aBinSample);

    virtual ~BinAudioFormatReader();

    const int getBitsPerFrame() const;
    const int getBytesPerFrame() const;
    const int getNumChannels() const;
    const int getSampleRate() const;
    const  int getDataChunkStart() const;
    const int getDataLength() const;
    const int getNumberOfSamples() const;

    //==============================================================================
    bool readSamples (int** destSamples, int numDestChannels, int startOffsetInDestBuffer,
                      int64 startSampleInFile, int numSamples) override;

    template <typename Endianness>
    static void copySampleData (unsigned int bitsPerSample, const bool usesFloatingPointData,
                                int* const* destSamples, int startOffsetInDestBuffer, int numDestChannels,
                                const void* sourceData, int numChannels, int numSamples) noexcept;

    static void copySampleData (unsigned int bitsPerSample, const bool usesFloatingPointData,
                                int* const* destSamples, int startOffsetInDestBuffer, int numDestChannels,
                                const void* sourceData, int numChannels, int numSamples) noexcept;

private:
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (BinAudioFormatReader)
};



/*--------------------------------------------------------------------------------------------------------------
 * MemoryMappedBinReader
 *
 *
 *
 ---------------------------------------------------------------------------------------------------------------*/

class MemoryMappedBinReader   : public MemoryMappedAudioFormatReader
{
public:
    MemoryMappedBinReader (const File& binFile, const BinAudioFormatReader& reader);

    bool readSamples (int** destSamples, int numDestChannels, int startOffsetInDestBuffer,
                      int64 startSampleInFile, int numSamples) override;
    void getSample (int64 sample, float* result) const noexcept override;
    void readMaxLevels (int64 startSampleInFile, int64 numSamples, Range<float>* results, int numChannelsToRead) override;

private:
    template <typename SampleType>
    void scanMinAndMax (int64 startSampleInFile, int64 numSamples, Range<float>* results, int numChannelsToRead) const noexcept;

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MemoryMappedBinReader)
};



#endif /* BinAudioFormat_hpp */
