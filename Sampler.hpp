/******************************************************************
 This software has been developed 2017 by Rudi Leitner
 as open source under the GPL3 license.
 
 It uses the open source version of JUCE GRAPEFRUIT under GPL3.
 *******************************************************************/
// Sampler.h

#ifndef SAMPLER_H_INCLUDED
#define SAMPLER_H_INCLUDED

#include "JuceHeader.h"
#include "PatchDefinition.hpp"
#include "PluginParameters.hpp"



/*--------------------------------------------------------------------------------------------------------------
 * RetriggerNoteEntry
 *
 *
 *
 ---------------------------------------------------------------------------------------------------------------*/
typedef struct  {
		int midiChannel;
		int midiNote;
		int layer;
		float velocity;
        SynthesiserSound* sound;
} RetriggerNoteEntry;



/*--------------------------------------------------------------------------------------------------------------
 * SimpleSamplerComponentAudioProcessorEditor
 * Forward declaration
 *
 *
 ---------------------------------------------------------------------------------------------------------------*/
class SimpleSamplerComponentAudioProcessorEditor;


/*--------------------------------------------------------------------------------------------------------------
 * Sampler
 *
 *
 *
 ---------------------------------------------------------------------------------------------------------------*/
class Sampler : public Synthesiser
{
private:
	PluginParameters* pluginParameters;

	int currentSoundSet;
	
	int keyMonoGroups[127];
    //int keyPressed[127];

	PatchDefinition* patchDefinition;
	SimpleSamplerComponentAudioProcessorEditor* editor;

	int measuredPressure;
	int previousMeasuredPressure;

    int currentPitchWheelValue;
	int measuredPitchSemitones;
	int previousMeasuredPitchSemitones;

	std::vector<RetriggerNoteEntry> retriggerNotes;


	int calcSemitonesFromPitchWheel( int pitchWheelValue, int pitchBendRangeSt );
    int calcPitchWheelFromSemiTones( int semiTones, int pitchBendRangeSt );
	void retriggerAllNotes( int midiChannel );
	int stopAllNotes( int midiChannel, float releaseT);
	int stopAllNotes( int midiChannel);

	int stopAllNotesInMonoGroup( int midiChannel, int aMonoGroup, float releaseT);
	int stopAllNotesInMonoGroup( int midiChannel, int aMonoGroup);


	bool isKeySwitch( int aMidiNote );
	KeyswitchDefinition* getKeySwitchDefinition( int aMidiNote );
	//std::string getKeySwitchName( int aType, int aParam);


public:

	Sampler( PluginParameters* aPluginParameters);

	void reset();
	void setEditor( SimpleSamplerComponentAudioProcessorEditor* aEditor);
	void setPatchDefinition( PatchDefinition* aPatchDefinition);


	virtual void noteOn (
			const int midiChannel,
			const int midiNoteNumber,
			const float velocity
	) override;


	virtual void noteOff (int midiChannel,
			int midiNoteNumber,
			float velocity,
			bool allowTailOff) override;



	virtual void handleController (int midiChannel,
			int controllerNumber,
			int controllerValue) override;

	virtual void handlePitchWheel (int midiChannel, int wheelValue) override;


};



#endif  // SAMPLER_H_INCLUDED
