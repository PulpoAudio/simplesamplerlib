/******************************************************************
 This software has been developed 2017 by Rudi Leitner
 as open source under the GPL3 license.
 
 It uses the open source version of JUCE GRAPEFRUIT under GPL3.
 *******************************************************************/
//
//  VelocityToGainMapper.cpp
//  SimpleSampler
//
//  Created by Rudolf Leitner on 23/09/16.
//
//

#include "VelocityToGainMapper.hpp"
#include <iostream>
#include <cmath>







/*--------------------------------------------------------------------------------------------------------------
 * VelocityToGainMapper
 *
 *
 *
 ---------------------------------------------------------------------------------------------------------------*/
float VelocityToGainMapper::velocityToGain( float iSensitivity, float iVelocity)
{
    float gain = iVelocity;
    if( iSensitivity <= 1.0) {
        gain = (iVelocity * iSensitivity + (1.0-iSensitivity));
    } else {
        gain = pow(iVelocity,iSensitivity );
    }
    
    return gain;
}




float VelocityToGainMapper::applyBevel( int keyRangeFrom, int keyRangeTo, int key, float bevel, float iVelocity)
{
    float factor = 1.0;
    float normKeyPos = (((float)(key-keyRangeFrom)) / ((float)(keyRangeTo-keyRangeFrom)));
    if( bevel < 0.0) {
        factor = 1.0 -((1.0-normKeyPos) * (-bevel));
    } else if (bevel > 0.0) {
        factor = 1.0-(normKeyPos * bevel);
    }
    return iVelocity * factor;
    
}


