/******************************************************************
 This software has been developed 2017 by Rudi Leitner
 as open source under the GPL3 license.
 
 It uses the open source version of JUCE GRAPEFRUIT under GPL3.
 *******************************************************************/
//
//  UIConfiguration.cpp
//  DryAged
//
//  Created by Rudolf Leitner on 20/02/17.
//
//

#include "UIConfiguration.hpp"


UIElementConfig::UIElementConfig()
{
    x=-1;
    y=-1;
    w=-1;
    h=-1;
    sizeX=-1;
    sizeY=-1;
    visible = true;
}

bool UIElementConfig::hasBounds()
{
    return (x>-1 && y>-1 && w>-1 && h>-1);
}
bool UIElementConfig::hasSize()
{
    return (sizeX>-1 && sizeY>-1);
}





void UIElementConfigs::addElementConfig( UIElementConfig& aElementConfig)
{
    elementConfigs.push_back(aElementConfig);
}

UIElementConfig* UIElementConfigs::getElementConfig( std::string aName)
{
    int n=elementConfigs.size();
    for( int i=0; i<n;i++) {
        UIElementConfig& uec = elementConfigs.at(i);
        if( uec.name == aName) {
            return &uec;
        }
    
    }
    return NULL;
}
