/******************************************************************
 This software has been developed 2017 by Rudi Leitner
 as open source under the GPL3 license.
 
 It uses the open source version of JUCE GRAPEFRUIT under GPL3.
 *******************************************************************/
//
//  UIConfiguration.hpp
//  DryAged
//
//  Created by Rudolf Leitner on 20/02/17.
//
//

#ifndef UIConfiguration_hpp
#define UIConfiguration_hpp

#include <stdio.h>
#include <string>
#include <vector>

class UIElementConfig {
public:
    std::string name;
    int x;
    int y;
    int w;
    int h;
    int sizeX;
    int sizeY;
    std::string param;
    bool visible;
    
    UIElementConfig();
    
    bool hasBounds();
    bool hasSize();
};

class UIElementConfigs {
    std::vector<UIElementConfig> elementConfigs;

public:
    int w;
    int h;

    void addElementConfig( UIElementConfig& aElementConfig);
    UIElementConfig* getElementConfig( std::string aName);
};



#endif /* UIConfiguration_hpp */
