/******************************************************************
 This software has been developed 2017 by Rudi Leitner
 as open source under the GPL3 license.
 
 It uses the open source version of JUCE GRAPEFRUIT under GPL3.
 *******************************************************************/
//
//  PatchDefinition.cpp
//  SimpleSampler
//
//  Created by Rudolf Leitner on 05/11/16.
//
//

#include "PatchDefinition.hpp"



/*--------------------------------------------------------------------------------------------------------------
 * SoundDefinition
 *
 *
 *
 ---------------------------------------------------------------------------------------------------------------*/
SoundDefinition::SoundDefinition( std::string soundName,
		int noteRangeFrom,
		int noteRangeTo,
		int centerNote,
		bool xFade,
		float gainDb,
        int tuneSt,
		float tuneCt,
		float aPan,
		int aLayer,
		int aSoundSet)
{
	set(soundName,noteRangeFrom,noteRangeTo,centerNote,xFade,gainDb,tuneSt,tuneCt,aPan,aLayer, aSoundSet);
};



SoundDefinition::SoundDefinition()
{
	set("",-1,-1,-1,false,0.0f,0,0.0f,0.0f,0,-1);
}

void SoundDefinition::set( std::string soundName,
		int noteRangeFrom,
		int noteRangeTo,
		int centerNote,
		bool xFade,
		float gainDb,
        int tuneSt,
		float tuneCt,
		float aPan,
		int aLayer,
		int aSoundSet)
{
	this->soundName = soundName;
	this->noteRangeFrom = noteRangeFrom;
	this->noteRangeTo = noteRangeTo;
	this->centerNote = centerNote;
	this->xFade = xFade;
	this->gainDb = gainDb;
    this->tuneSt = tuneSt;
	this->tuneCt = tuneCt;
	this->pan = aPan;
	this->layer = aLayer;
	this->soundSet = aSoundSet;
}




void SoundDefinition::addSoundElementDefinition( SoundElementDefinition& aSoundElementDefinition)
{
	soundElementDefinitions.push_back(aSoundElementDefinition);
}


void SoundDefinition::addSoundElementDefinition( std::string aSoundName, float aGainDb, float aVelocityRangeFrom, float aVelocityRangeTo, float aXFadeVelocityTo)
{
	SoundElementDefinition sed( aGainDb, aVelocityRangeFrom, aVelocityRangeTo, aXFadeVelocityTo);
	sed.addRoundRobinSound( aSoundName );
	soundElementDefinitions.push_back(sed);
}


void SoundDefinition::addSoundElementDefinition( std::string aSoundNameRR1, std::string aSoundNameRR2,float aGainDb, float aVelocityRangeFrom, float aVelocityRangeTo, float aXFadeVelocityTo )
{
	SoundElementDefinition sed( aGainDb, aVelocityRangeFrom, aVelocityRangeTo, aXFadeVelocityTo);
	sed.addRoundRobinSound( aSoundNameRR1 );
	sed.addRoundRobinSound( aSoundNameRR2 );
	soundElementDefinitions.push_back(sed);
}
void SoundDefinition::addSoundElementDefinition( std::string aSoundNameRR1, std::string aSoundNameRR2, std::string aSoundNameRR3,float aGainDb, float aVelocityRangeFrom, float aVelocityRangeTo, float aXFadeVelocityTo )
{
	SoundElementDefinition sed( aGainDb, aVelocityRangeFrom, aVelocityRangeTo, aXFadeVelocityTo);
	sed.addRoundRobinSound( aSoundNameRR1 );
	sed.addRoundRobinSound( aSoundNameRR2 );
	sed.addRoundRobinSound( aSoundNameRR3 );
	soundElementDefinitions.push_back(sed);
}
void SoundDefinition::addSoundElementDefinition( std::string aSoundNameRR1, std::string aSoundNameRR2, std::string aSoundNameRR3, std::string aSoundNameRR4, float aGainDb, float aVelocityRangeFrom, float aVelocityRangeTo, float aXFadeVelocityTo )
{
	SoundElementDefinition sed( aGainDb, aVelocityRangeFrom, aVelocityRangeTo, aXFadeVelocityTo);
	sed.addRoundRobinSound( aSoundNameRR1 );
	sed.addRoundRobinSound( aSoundNameRR2 );
	sed.addRoundRobinSound( aSoundNameRR4 );
	soundElementDefinitions.push_back(sed);
}



void SoundDefinition::addSoundElementDefinition( std::string aSoundName, int xFadeSlot, int nXFadeSlots )
{
	float slotWidth = 1.0f / nXFadeSlots;
	float veloRangeFrom = xFadeSlot * slotWidth;
	float veloRangeTo = veloRangeFrom + slotWidth;
	float veloRangeXFadeTo = std::min(veloRangeTo + slotWidth,1.0f);
	
	SoundElementDefinition sed( 0.0f, veloRangeFrom, veloRangeTo, veloRangeXFadeTo);
	sed.addRoundRobinSound( aSoundName );
	soundElementDefinitions.push_back(sed);
}	

void SoundDefinition::addSoundElementDefinition( std::string aSoundNameRR1, std::string aSoundNameRR2,int xFadeSlot, int nXFadeSlots )
{
	float slotWidth = 1.0f / nXFadeSlots;
	float veloRangeFrom = xFadeSlot * slotWidth;
	float veloRangeTo = veloRangeFrom + slotWidth;
	float veloRangeXFadeTo = std::min(veloRangeTo + slotWidth,1.0f);
	
	SoundElementDefinition sed( 0.0f, veloRangeFrom, veloRangeTo, veloRangeXFadeTo);
	sed.addRoundRobinSound( aSoundNameRR1 );
	sed.addRoundRobinSound( aSoundNameRR2 );
	soundElementDefinitions.push_back(sed);
}	

void SoundDefinition::addSoundElementDefinition( std::string aSoundNameRR1, std::string aSoundNameRR2, std::string aSoundNameRR3,int xFadeSlot, int nXFadeSlots )
{
	float slotWidth = 1.0f / nXFadeSlots;
	float veloRangeFrom = xFadeSlot * slotWidth;
	float veloRangeTo = veloRangeFrom + slotWidth;
	float veloRangeXFadeTo = std::min(veloRangeTo + slotWidth,1.0f);
	
	SoundElementDefinition sed( 0.0f, veloRangeFrom, veloRangeTo, veloRangeXFadeTo);
	sed.addRoundRobinSound( aSoundNameRR1 );
	sed.addRoundRobinSound( aSoundNameRR2 );
	sed.addRoundRobinSound( aSoundNameRR3 );
	soundElementDefinitions.push_back(sed);
}	

void SoundDefinition::addSoundElementDefinition( std::string aSoundNameRR1, std::string aSoundNameRR2, std::string aSoundNameRR3, std::string aSoundNameRR4, int xFadeSlot, int nXFadeSlots )
{
	float slotWidth = 1.0f / nXFadeSlots;
	float veloRangeFrom = xFadeSlot * slotWidth;
	float veloRangeTo = veloRangeFrom + slotWidth;
	float veloRangeXFadeTo = std::min(veloRangeTo + slotWidth,1.0f);
	
	SoundElementDefinition sed( 0.0f, veloRangeFrom, veloRangeTo, veloRangeXFadeTo);
	sed.addRoundRobinSound( aSoundNameRR1 );
	sed.addRoundRobinSound( aSoundNameRR2 );
	sed.addRoundRobinSound( aSoundNameRR3 );
	sed.addRoundRobinSound( aSoundNameRR4 );
	soundElementDefinitions.push_back(sed);
}	

/*--------------------------------------------------------------------------------------------------------------
 * AHDSREnvelopeDefinition
 *
 *
 *
 ---------------------------------------------------------------------------------------------------------------*/

AHDSREnvelopeDefinition::AHDSREnvelopeDefinition(float aOffsetT,
		float aAttackT,
		float aHoldT,
		float aDecayT,
		float aSustainDb,
		float aReleaseT)
{
	set( aOffsetT, aAttackT, aHoldT, aDecayT, aSustainDb, aReleaseT );
}


AHDSREnvelopeDefinition::AHDSREnvelopeDefinition()
{
	set(0,0,0,0,0,0);
}

void AHDSREnvelopeDefinition::set (float aOffsetT,
		float aAttackT,
		float aHoldT,
		float aDecayT,
		float aSustainDb,
		float aReleaseT)
{
	offsetT = aOffsetT;
	attackT = aAttackT;
	holdT = aHoldT;
	decayT = aDecayT;
	sustainDb = aSustainDb;
	releaseT = aReleaseT;
}




/*--------------------------------------------------------------------------------------------------------------
 * KeyswitchDefinition
 *
 *
 *
 ---------------------------------------------------------------------------------------------------------------*/

KeyswitchDefinition::KeyswitchDefinition( std::string aName, int aKey, bool aIsPermanent, KEYSWITCHTYPE aType, int aParam)
{
    name = aName;
    key = aKey;
    type = aType;
    param = aParam;
    isPermanent = aIsPermanent;
}


/*--------------------------------------------------------------------------------------------------------------
 * KeyGroupDefinition
 *
 *
 *
 ---------------------------------------------------------------------------------------------------------------*/

KeyGroupDefinition::KeyGroupDefinition( bool aIsMono, int aFromKey, int aToKey, float aRelGainDb, float aRelPan, int aRelTuneSt, float aRelTuneCt)
{
    isMono = aIsMono;
    fromKey = aFromKey;
    toKey = aToKey;
    relGainDb = aRelGainDb;
    relPan = aRelPan;
    relTuneSt = aRelTuneSt;
    relTuneCt = aRelTuneCt;
    
}





/*--------------------------------------------------------------------------------------------------------------
 * PatchDefinition
 *
 *
 *
 ---------------------------------------------------------------------------------------------------------------*/

PatchDefinition::PatchDefinition(std::string aName)
{
	init();
	name = aName;
	numberOfVoices = 0;
}

PatchDefinition::PatchDefinition()
{
	init();
}

void PatchDefinition::init()
{
	name = "";
	numberOfVoices = 0;
	gainDb = 0.0f;
	pan = 0.0f;
	tune = 0.0f;
    tuneSt = 0;
	veloSens = 1.0;
    bevel = 0.0;
	keySwitchRangeFromMidiNote = 0;
	keySwitchRangeToMidiNote = 0;
	playRangeFromMidiNote = 0;
	playRangeToMidiNote = 127;
    bevelRangeFromMidiNote = 0;
    bevelRangeToMidiNote = 127;
	pressureMode = PRESSUREMODE::VELOCITY;
	pressureController = 1;
	layer1GainDb = 0.0f;
	layer2GainDb = 0.0f;
	layer3GainDb = 0.0f;
	layer4GainDb = 0.0f;
	pitchWheelMode = PITCHWHEELMODE::CONTINUOUS;
	pitchWheelRangeSt = 12;
	pitchWheelRetriggerReleaseS = 0.0f;
	pitchWheelRetriggerAttackS = 0.0f;
	pitchWheelRetriggerOffsetS = 0.0f;

	legatoMode = LEGATOMODE::OFF;
	legatoReleaseS = 0.0f;
	legatoOffsetS = 0.0f;
	legatoAttackS = 0.0f;

	monoMode = false;
	dfd = false;
	dfdPreloadFrames = 0.0;
    dfdLoadFrames = 0.0;
    dfdNrOfRingBuffers = 0;
}


PatchDefinition::~PatchDefinition()
{
}



void PatchDefinition::setName( std::string aName)
{
	name = aName;
}

void PatchDefinition::addSoundDefinition( SoundDefinition& aSoundDefinition)
{
	soundDefinitions.push_back(aSoundDefinition);
}


SoundDefinition* PatchDefinition::createAndAddSoundDefinition()
{
	SoundDefinition sd;
	soundDefinitions.push_back(sd);

	return &soundDefinitions[ soundDefinitions.size()-1];

}


void PatchDefinition::addKeyswitchDefinition( KeyswitchDefinition& aKeyswitchDefinition)
{
	keyswitchDefinitions.push_back(aKeyswitchDefinition);
	if( aKeyswitchDefinition.type == KeyswitchDefinition::KEYSWITCHTYPE_SOUNDSET) {
		soundSetNames.add( aKeyswitchDefinition.name );
	}
}

void PatchDefinition::addKeyswitchDefinition( std::string aName, int aMidiNote, bool aIsPermanent, KeyswitchDefinition::KEYSWITCHTYPE aType, int aParam)
{
	KeyswitchDefinition ks( aName, aMidiNote, aIsPermanent, aType, aParam);
	addKeyswitchDefinition(ks);
}


void PatchDefinition::setEnvelopeDefinition(AHDSREnvelopeDefinition& aEnvelopeDefinition)
{
	envelopeDefinition = aEnvelopeDefinition;
}

void PatchDefinition::setEnvelopeDefinition(float aOffsetT, float aAttackT, float aHoldT, float aDecayT, float aSustainDb, float aReleaseT)
{
	envelopeDefinition.set(aOffsetT, aAttackT, aHoldT, aDecayT, aSustainDb, aReleaseT);
}



int PatchDefinition::getNrOfSoundDefinitions()
{
	return soundDefinitions.size();
}

SoundDefinition& PatchDefinition::getSoundDefinitionAt( int ndx )
{
	return soundDefinitions.at( ndx );
}


AHDSREnvelopeDefinition& PatchDefinition::getEnvelopeDefinition()
{
	return envelopeDefinition;
}

void PatchDefinition::setPitchWheelDefinition( PITCHWHEELMODE aPitchWheelMode, int aPitchWheelRangeSt, float aRetriggerReleaseS, float aRetriggerOffsetS, float aRetriggerAttackS )
{
	pitchWheelMode = aPitchWheelMode;
	pitchWheelRangeSt = aPitchWheelRangeSt;
	pitchWheelRetriggerReleaseS = aRetriggerReleaseS;
	pitchWheelRetriggerOffsetS = aRetriggerOffsetS;
	pitchWheelRetriggerAttackS = aRetriggerAttackS;
}		


void PatchDefinition::setLegatoDefinition( LEGATOMODE aLegatoMode, float aLegatoReleaseS, float aLegatoOffsetS, float aLegatoAttackS )
{
	legatoMode = aLegatoMode;
	legatoReleaseS = aLegatoReleaseS;
	legatoOffsetS = aLegatoOffsetS;
	legatoAttackS = aLegatoAttackS;
}



void PatchDefinition::setDfd( bool aDfd, int aNrOfRingBuffers, int aDfdPreloadFrames, int aDfdLoadFrames, int aDfdLoadAtFramesLeft)
{
	dfd = aDfd;
	dfdPreloadFrames = aDfdPreloadFrames;
    dfdLoadFrames = aDfdLoadFrames;
    dfdNrOfRingBuffers = aNrOfRingBuffers;
    dfdLoadAtFramesLeft = aDfdLoadAtFramesLeft;
}

void PatchDefinition::addMonoGroupRange( int fromNoteNr, int toNoteNr )
{
	Range<int> r(fromNoteNr, toNoteNr);
	monoGroupRanges.push_back( r );
}


void PatchDefinition::addKeyGroup( bool aIsMono, int aFromKey, int aToKey, float aRelGainDb, float aRelPan, int aRelTuneSt, float aRelTuneCt )
{
    KeyGroupDefinition kgd( aIsMono, aFromKey, aToKey, aRelGainDb, aRelPan, aRelTuneSt, aRelTuneCt);
    keyGroups.push_back(kgd);
    
}


