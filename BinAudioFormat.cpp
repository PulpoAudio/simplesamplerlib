/******************************************************************
 This software has been developed 2017 by Rudi Leitner
 as open source under the GPL3 license.
 
 It uses the open source version of JUCE GRAPEFRUIT under GPL3.
 *******************************************************************/
//
//  BinAudioFormat.cpp
//  SimpleSamplerComponent
//
//  Created by Rudolf Leitner on 26/12/16.
//
//

#include "BinAudioFormat.hpp"
#include "SampleBin.hpp" 




static const char* const binFormatName = "BIN file";


/*--------------------------------------------------------------------------------------------------------------
 * BinAudioFormat
 *
 * Audio Format, FormatReader and MemoryMappedReader for a BIN file.
 * A BIN file carries a block with information for each sample  (samplerate, channels, startbyte, length, etc)
 * and afterwards the sample frames for all samples.
 *
 * Using the BinSample as description ensures that the specific byte range (from..to) of the sample bin is
 * treated like one wav/aif file.
 ---------------------------------------------------------------------------------------------------------------*/

//==============================================================================
BinAudioFormat::BinAudioFormat(SampleBinSample* aBinSample)
: AudioFormat (binFormatName, ".bin")
{
	binSample = aBinSample;
};

BinAudioFormat::~BinAudioFormat() {};

Array<int> BinAudioFormat::getPossibleSampleRates()
{
	const int rates[] = { 8000, 11025, 12000, 16000, 22050, 32000, 44100,
			48000, 88200, 96000, 176400, 192000, 352800, 384000 };

	return Array<int> (rates, numElementsInArray (rates));
}

Array<int> BinAudioFormat::getPossibleBitDepths()
{
	const int depths[] = { 8, 16, 24, 32 };

	return Array<int> (depths, numElementsInArray (depths));
}

bool BinAudioFormat::canDoStereo()  { return true; }
bool BinAudioFormat::canDoMono()    { return true; }

AudioFormatReader* BinAudioFormat::createReaderFor (InputStream* sourceStream,
		const bool deleteStreamIfOpeningFails)
{
	ScopedPointer<BinAudioFormatReader> r (new BinAudioFormatReader (sourceStream, binSample));

	if (r->getSampleRate() > 0 && r->getNumChannels ()> 0 && r->getBytesPerFrame() > 0)
		return r.release();

	if (! deleteStreamIfOpeningFails)
		r->input = nullptr;

	return nullptr;
}


MemoryMappedAudioFormatReader* BinAudioFormat::createMemoryMappedReader (const File& file)
{
	if (ScopedPointer<FileInputStream> fin = file.createInputStream())
	{
		BinAudioFormatReader reader (fin, binSample);

		if (reader.lengthInSamples > 0)
			return new MemoryMappedBinReader (file, reader);
	}

	return nullptr;
}

AudioFormatWriter* BinAudioFormat::createWriterFor (OutputStream* out, double sampleRate,
		unsigned int numChannels, int bitsPerSample,
		const StringPairArray& metadataValues, int /*qualityOptionIndex*/)
{
	// we do not need to write
	return nullptr;
}





/*--------------------------------------------------------------------------------------------------------------
 * BinAudioFormatReader
 *
 *
 *
 ---------------------------------------------------------------------------------------------------------------*/

BinAudioFormatReader::BinAudioFormatReader (InputStream* const in, SampleBinSample* aBinSample)
: AudioFormatReader (nullptr, binFormatName), binSample(aBinSample)
{
    binInputStream = in;
	bitsPerSample = getBitsPerFrame();
	sampleRate = getSampleRate();
	lengthInSamples = getNumberOfSamples();
	numChannels = getNumChannels();
	usesFloatingPointData = ( bitsPerSample == 32);
}

BinAudioFormatReader::~BinAudioFormatReader()
{
    //if( binInputStream != NULL) {
        //delete binInputStream;
    //}
};

const int BinAudioFormatReader::getBitsPerFrame() const {
	if(binSample != NULL) {
		return binSample->bitsPerFrame;
	}
	return 0;
}


const int BinAudioFormatReader::getBytesPerFrame() const {
	if(binSample != NULL) {
		return binSample->bitsPerFrame/8;
	}
	return 0;
}

const int BinAudioFormatReader::getNumChannels() const {
	if(binSample != NULL) {
		return binSample->numberOfChannels;
	}
	return 0;
}

const int BinAudioFormatReader::getSampleRate() const {
	if(binSample != NULL) {
		return binSample->sampleRate;
	}
	return 0;
}

const  int BinAudioFormatReader::getDataChunkStart() const {
	if(binSample != NULL) {
		return binSample->startsAtByteInBin;
	}
	return -1;
}


const int BinAudioFormatReader::getDataLength() const {
	if(binSample != NULL) {
		return binSample->numberOfFrames*binSample->bitsPerFrame/8;
	}
	return -1;

}

const int BinAudioFormatReader::getNumberOfSamples() const {
	if(binSample != NULL) {
		return binSample->numberOfFrames;
	}
	return -1;

}


bool BinAudioFormatReader::readSamples (int** destSamples, int numDestChannels, int startOffsetInDestBuffer,
				int64 startSampleInFile, int numSamples)
{
	clearSamplesBeyondAvailableLength (destSamples, numDestChannels, startOffsetInDestBuffer,
			startSampleInFile, numSamples, lengthInSamples);

	if (numSamples <= 0 && binSample == NULL)
		return true;

	int bytesPerFrame = binSample->bitsPerFrame / 8;
	binInputStream->setPosition ( binSample->startsAtByteInBin + startSampleInFile * bytesPerFrame);

	while (numSamples > 0)
	{
		const int tempBufSize = 480 * 3 * 4; // (keep this a multiple of 3)
		char tempBuffer [tempBufSize];

		const int numThisTime = jmin (tempBufSize / bytesPerFrame, numSamples);
		const int bytesRead = binInputStream->read (tempBuffer, numThisTime * bytesPerFrame);

		if (bytesRead < numThisTime * bytesPerFrame)
		{
			jassert (bytesRead >= 0);
			zeromem (tempBuffer + bytesRead, (size_t) (numThisTime * bytesPerFrame - bytesRead));
		}

		copySampleData<AudioData::BigEndian> (bitsPerSample, usesFloatingPointData,
				destSamples, startOffsetInDestBuffer, numDestChannels,
				tempBuffer, (int) numChannels, numThisTime);

		startOffsetInDestBuffer += numThisTime;
		numSamples -= numThisTime;
	}

	return true;
}

template <typename Endianness>
void BinAudioFormatReader::copySampleData (unsigned int bitsPerSample, const bool usesFloatingPointData,
		int* const* destSamples, int startOffsetInDestBuffer, int numDestChannels,
		const void* sourceData, int numChannels, int numSamples) noexcept
{
	switch (bitsPerSample)
	{
	case 8:     ReadHelper<AudioData::Int32, AudioData::Int8,  Endianness>::read (destSamples, startOffsetInDestBuffer, numDestChannels, sourceData, numChannels, numSamples); break;
	case 16:    ReadHelper<AudioData::Int32, AudioData::Int16, Endianness>::read (destSamples, startOffsetInDestBuffer, numDestChannels, sourceData, numChannels, numSamples); break;
	case 24:    ReadHelper<AudioData::Int32, AudioData::Int24, Endianness>::read (destSamples, startOffsetInDestBuffer, numDestChannels, sourceData, numChannels, numSamples); break;
	case 32:    if (usesFloatingPointData) ReadHelper<AudioData::Float32, AudioData::Float32, Endianness>::read (destSamples, startOffsetInDestBuffer, numDestChannels, sourceData, numChannels, numSamples);
	else                       ReadHelper<AudioData::Int32,   AudioData::Int32,   Endianness>::read (destSamples, startOffsetInDestBuffer, numDestChannels, sourceData, numChannels, numSamples); break;
	default:    jassertfalse; break;
	}
}

 void BinAudioFormatReader::copySampleData (unsigned int bitsPerSample, const bool usesFloatingPointData,
		int* const* destSamples, int startOffsetInDestBuffer, int numDestChannels,
		const void* sourceData, int numChannels, int numSamples) noexcept
{
	switch (bitsPerSample)
	{
	case 8:     ReadHelper<AudioData::Int32, AudioData::UInt8, AudioData::LittleEndian>::read (destSamples, startOffsetInDestBuffer, numDestChannels, sourceData, numChannels, numSamples); break;
	case 16:    ReadHelper<AudioData::Int32, AudioData::Int16, AudioData::LittleEndian>::read (destSamples, startOffsetInDestBuffer, numDestChannels, sourceData, numChannels, numSamples); break;
	case 24:    ReadHelper<AudioData::Int32, AudioData::Int24, AudioData::LittleEndian>::read (destSamples, startOffsetInDestBuffer, numDestChannels, sourceData, numChannels, numSamples); break;
	case 32:    if (usesFloatingPointData) ReadHelper<AudioData::Float32, AudioData::Float32, AudioData::LittleEndian>::read (destSamples, startOffsetInDestBuffer, numDestChannels, sourceData, numChannels, numSamples);
	else                       ReadHelper<AudioData::Int32,   AudioData::Int32,   AudioData::LittleEndian>::read (destSamples, startOffsetInDestBuffer, numDestChannels, sourceData, numChannels, numSamples); break;
	default:    jassertfalse; break;
	}
}







/*--------------------------------------------------------------------------------------------------------------
 * MemoryMappedBinReader
 *
 *
 *
 ---------------------------------------------------------------------------------------------------------------*/

MemoryMappedBinReader::MemoryMappedBinReader (const File& binFile, const BinAudioFormatReader& reader)
: MemoryMappedAudioFormatReader (binFile, reader, reader.getDataChunkStart(),
		reader.getDataLength(), reader.getBytesPerFrame())
{
}

bool MemoryMappedBinReader::readSamples (int** destSamples, int numDestChannels, int startOffsetInDestBuffer,
		int64 startSampleInFile, int numSamples)
{
	clearSamplesBeyondAvailableLength (destSamples, numDestChannels, startOffsetInDestBuffer,
			startSampleInFile, numSamples, lengthInSamples);

	if (map == nullptr || ! mappedSection.contains (Range<int64> (startSampleInFile, startSampleInFile + numSamples)))
	{
		jassertfalse; // you must make sure that the window contains all the samples you're going to attempt to read.
		return false;
	}

	BinAudioFormatReader::copySampleData<AudioData::BigEndian> (bitsPerSample, usesFloatingPointData,
			destSamples, startOffsetInDestBuffer, numDestChannels,
			sampleToPointer (startSampleInFile), (int) numChannels, numSamples);
	return true;
}

void MemoryMappedBinReader::getSample (int64 sample, float* result) const noexcept 
		{
	const int num = (int) numChannels;

	if (map == nullptr || ! mappedSection.contains (sample))
	{
		jassertfalse; // you must make sure that the window contains all the samples you're going to attempt to read.

		zeromem (result, sizeof (float) * (size_t) num);
		return;
	}

	float** dest = &result;
	const void* source = sampleToPointer (sample);

	switch (bitsPerSample)
	{
	case 8:     ReadHelper<AudioData::Float32, AudioData::UInt8, AudioData::BigEndian>::read (dest, 0, 1, source, 1, num); break;
	case 16:    ReadHelper<AudioData::Float32, AudioData::Int16, AudioData::BigEndian>::read (dest, 0, 1, source, 1, num); break;
	case 24:    ReadHelper<AudioData::Float32, AudioData::Int24, AudioData::BigEndian>::read (dest, 0, 1, source, 1, num); break;
	case 32:    if (usesFloatingPointData) ReadHelper<AudioData::Float32, AudioData::Float32, AudioData::BigEndian>::read (dest, 0, 1, source, 1, num);
	else                       ReadHelper<AudioData::Float32, AudioData::Int32,   AudioData::BigEndian>::read (dest, 0, 1, source, 1, num); break;
	default:    jassertfalse; break;
	}
		}

void MemoryMappedBinReader::readMaxLevels (int64 startSampleInFile, int64 numSamples, Range<float>* results, int numChannelsToRead)
{
	numSamples = jmin (numSamples, lengthInSamples - startSampleInFile);

	if (map == nullptr || numSamples <= 0 || ! mappedSection.contains (Range<int64> (startSampleInFile, startSampleInFile + numSamples)))
	{
		jassert (numSamples <= 0); // you must make sure that the window contains all the samples you're going to attempt to read.

		for (int i = 0; i < numChannelsToRead; ++i)
			results[i] = Range<float>();

		return;
	}

	switch (bitsPerSample)
	{
	case 8:     scanMinAndMax<AudioData::UInt8> (startSampleInFile, numSamples, results, numChannelsToRead); break;
	case 16:    scanMinAndMax<AudioData::Int16> (startSampleInFile, numSamples, results, numChannelsToRead); break;
	case 24:    scanMinAndMax<AudioData::Int24> (startSampleInFile, numSamples, results, numChannelsToRead); break;
	case 32:    if (usesFloatingPointData) scanMinAndMax<AudioData::Float32> (startSampleInFile, numSamples, results, numChannelsToRead);
	else                       scanMinAndMax<AudioData::Int32>   (startSampleInFile, numSamples, results, numChannelsToRead); break;
	default:    jassertfalse; break;
	}
}

template <typename SampleType>
void MemoryMappedBinReader::scanMinAndMax (int64 startSampleInFile, int64 numSamples, Range<float>* results, int numChannelsToRead) const noexcept
{
	for (int i = 0; i < numChannelsToRead; ++i)
		results[i] = scanMinAndMaxInterleaved<SampleType, AudioData::BigEndian> (i, startSampleInFile, numSamples);
}






