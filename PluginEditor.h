/******************************************************************
 This software has been developed 2017 by Rudi Leitner
 as open source under the GPL3 license.
 
 It uses the open source version of JUCE GRAPEFRUIT under GPL3.
 *******************************************************************/
/*
  ==============================================================================

    This file was auto-generated!

    It contains the basic framework code for a JUCE plugin editor.

  ==============================================================================
*/

#ifndef PLUGINEDITOR_H_INCLUDED
#define PLUGINEDITOR_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"
#include "PluginProcessor.h"
#include "UIControls.hpp"
#include "PatchDefinition.hpp"
#include "ProductDef.h"



//==============================================================================
/**
*/
class SimpleSamplerComponentAudioProcessorEditor  : public AudioProcessorEditor,
													private Button::Listener
{
public:
    SimpleSamplerComponentAudioProcessorEditor (SimpleSamplerComponentAudioProcessor&, UIElementConfigs* aUIConfigurations);
    ~SimpleSamplerComponentAudioProcessorEditor();

    //==============================================================================
    void paint (Graphics&) override;
    void resized() override;
    void configureComponents();

    void setPatchDefinition(PatchDefinition* aPatchDefinition);
    void addLabel( Label& aLabel, std::string aName);
    void configureLabel( Label& aLabel, std::string aName);
    void addSlider( Slider& aSlider, std::string aName);
    void configureSlider( Slider& aSlider, std::string aName);
    void addComboBox( ComboBox& aCombo, std::string aName);
    void configureComboBox( ComboBox& aCombo, std::string aName);
    void addButton( Button& aButton, std::string aName);
    void configureButton( Button& aButton, std::string aName);
    void addComponent( Component& aComponent, std::string aName);
    void configureComponent( Component& aComponent, std::string aName);
    
private:
    std::string licInfo;
    MyLookAndFeel lookAndFeel;
    UIUpdateTimer uiUpdateTimer;
    juce::Image rotarySliderKnobManImage;
    juce::Image toggleButtonKnobManImage;
    juce::Image backgroundImg;


    // This reference is provided as a quick way for your editor to
    // access the processor object that created it.
    SimpleSamplerComponentAudioProcessor& processor;

    PatchDefinition* patchDefinition;
    UIElementConfigs* uiConfigurations;

    Label gainLabel, panLabel, tuneStLabel, tuneLabel, veloSensLabel, bevelLabel;
	Label offsetTLabel, attackTLabel, holdTLabel, decayTLabel, sustainDbLabel, releaseTLabel;
	Label layer1Label, layer2Label, layer3Label, layer4Label;
    
    
    Label pressureModeLabel, pressureControllerLabel;
	Label pitchWheelModeLabel, pitchWheelRangeLabel, pitchWheelReleaseTLabel, pitchWheelOffsetTLabel, pitchWheelAttackTLabel;
	Label legatoModeLabel, legatoReleaseTLabel,legatoOffsetTLabel,legatoAttackTLabel;
	Label keySwitchRangeShiftlabel, playRangeShiftLabel, monoModeLabel, dfdLabel;
	Label soundSetLabel;
    
	ScopedPointer<TextButton> reloadButton;
    ScopedPointer<TextButton> aboutButton;
    
    ScopedPointer<ParameterKeyRangeMidiKeyboardComponent> midiKeyboard;
    
    ScopedPointer<DBParameterSlider> gainSlider;
    ScopedPointer<PrcParameterSlider> panSlider;
    ScopedPointer<ParameterSlider> tuneStSlider;
    ScopedPointer<ParameterSlider> tuneSlider;
    ScopedPointer<ParameterSlider> veloSensSlider;
    ScopedPointer<ParameterSlider> bevelSlider;
    
    ScopedPointer<ParameterSlider> offsetTSlider;
    ScopedPointer<ParameterSlider> attackTSlider;
    ScopedPointer<ParameterSlider> holdTSlider;
    ScopedPointer<ParameterSlider> decayTSlider;
    ScopedPointer<DBParameterSlider> sustainDbSlider;
    ScopedPointer<ParameterSlider> releaseTSlider;
    
    ScopedPointer<DBParameterSlider> layer1GainSlider;
    ScopedPointer<DBParameterSlider> layer2GainSlider;
    ScopedPointer<DBParameterSlider> layer3GainSlider;
    ScopedPointer<DBParameterSlider> layer4GainSlider;
    
    ScopedPointer<ParameterChoiceComboBox> pressureModeCombo;
    ScopedPointer<ParameterChoiceComboBox> pressureControllerCombo;

    ScopedPointer<ParameterChoiceComboBox> pitchWheelModeCombo;
    ScopedPointer<ParameterSlider> pitchWheelRangeSlider;
    ScopedPointer<ParameterSlider> pitchWheelRetriggerReleaseTSlider;
    ScopedPointer<ParameterSlider> pitchWheelRetriggerOffsetTSlider;
    ScopedPointer<ParameterSlider> pitchWheelRetriggerAttackTSlider;

    ScopedPointer<ParameterChoiceComboBox> legatoModeCombo;
    ScopedPointer<ParameterSlider> legatoReleaseTSlider;
    ScopedPointer<ParameterSlider> legatoOffsetTSlider;
    ScopedPointer<ParameterSlider> legatoAttackTSlider;

    ScopedPointer<ParameterSlider> keySwitchRangeShiftSlider;
    ScopedPointer<ParameterSlider> playRangeShiftSlider;
    
    ScopedPointer<ParameterToggleButton> monoModeToggleButton;
    ScopedPointer<ParameterToggleButton> dfdToggleButton;
    
    ScopedPointer<ParameterIntComboBox> soundSetCombo;

    
    virtual void buttonClicked (Button* buttonThatWasClicked) override;

    
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (SimpleSamplerComponentAudioProcessorEditor)
};


#endif  // PLUGINEDITOR_H_INCLUDED
