/******************************************************************
 This software has been developed 2017 by Rudi Leitner
 as open source under the GPL3 license.
 
 It uses the open source version of JUCE GRAPEFRUIT under GPL3.
 *******************************************************************/
//
//  PatchCreator.hpp
//  SimpleSampler
//
//  Created by Rudolf Leitner on 05/11/16.
//
//

#ifndef PatchCreator_hpp
#define PatchCreator_hpp

#include <stdio.h>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include "PatchDefinition.hpp"
#include "UIConfiguration.hpp"
#include "JuceHeader.h"


/*--------------------------------------------------------------------------------------------------------------
 * AHDSREnvelopeDefinition
 *
 *
 *
 ---------------------------------------------------------------------------------------------------------------*/

class Configurator  {
private:

	PatchDefinition* createPatchFromXML( XmlElement* xmlPatch );
    UIElementConfig createElementConfigFromXML( XmlElement* xmlUiElement );

public:
	Configurator();

    bool configureFromXMLFile(  std::vector<PatchDefinition*>& patchDefinitions, UIElementConfigs& uiConfigurations, std::string aPath );
	bool configureFromXML(  std::vector<PatchDefinition*>& patchDefinitions, UIElementConfigs& uiConfigurations, std::string aXML );
	
};



#endif /* PatchDefinition_hpp */




