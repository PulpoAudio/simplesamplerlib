/******************************************************************
 This software has been developed 2017 by Rudi Leitner
 as open source under the GPL3 license.
 
 It uses the open source version of JUCE GRAPEFRUIT under GPL3.
 *******************************************************************/
// Sampler.cpp

#include "Sampler.hpp"
#include "XFadeSamplerSound.hpp"
#include "XFadeSamplerVoice.hpp"
#include "VelocityToGainMapper.hpp"
#include "PlugInEditor.h"
#include <sstream>



/*--------------------------------------------------------------------------------------------------------------
 * Sampler
 *
 *
 *
 ---------------------------------------------------------------------------------------------------------------*/
Sampler::Sampler( PluginParameters* aPluginParameters)
{
	pluginParameters = aPluginParameters;
	patchDefinition = NULL;
	editor = NULL;

	reset();
}

void Sampler::reset()
{
	measuredPressure = 0;
	previousMeasuredPressure = 0;

    currentPitchWheelValue = 8192;
	measuredPitchSemitones = 0;
	previousMeasuredPitchSemitones =0;

	currentSoundSet = NULL;
	
	for( int i=0; i<127; i++) {
		keyMonoGroups[i] = -1;
	}
    //for( int i=0; i<127; i++) {
    //    keyPressed[i] = 0;
    //}
	
	
}

void Sampler::setEditor( SimpleSamplerComponentAudioProcessorEditor* aEditor)
{
	editor = aEditor;
}


void Sampler::setPatchDefinition( PatchDefinition* aPatchDefinition)
{
    reset();
	patchDefinition = aPatchDefinition;
	
	// Setup a 127 keys wide array (piano keyboard) which describes 
	// which key is part of which mono group
    if( aPatchDefinition != nullptr) {
        for( int i = 0; i<patchDefinition->monoGroupRanges.size(); i++ ){
            Range<int>& r = patchDefinition->monoGroupRanges[i];
            for( int j=r.getStart(); j<=r.getEnd(); j++) {
                keyMonoGroups[j] = i;
            }
        }
    }
}


/*
 * 		KEYSWITCH HANDLING
 */

bool Sampler::isKeySwitch( int aMidiNote )
{
	int ksRangeFrom = patchDefinition->keySwitchRangeFromMidiNote + *pluginParameters->keySwitchRangeShiftParam;
	int ksRangeTo = patchDefinition->keySwitchRangeToMidiNote + *pluginParameters->keySwitchRangeShiftParam;
	return( aMidiNote >= ksRangeFrom && aMidiNote <= ksRangeTo);

}

KeyswitchDefinition* Sampler::getKeySwitchDefinition( int aMidiNote )
{
	int ksRangeFrom = patchDefinition->keySwitchRangeFromMidiNote + *pluginParameters->keySwitchRangeShiftParam;
	int ksRangeTo = patchDefinition->keySwitchRangeToMidiNote + *pluginParameters->keySwitchRangeShiftParam;
	if( aMidiNote >= ksRangeFrom && aMidiNote <= ksRangeTo)
	{
		int key = aMidiNote - ksRangeFrom;

		int n=patchDefinition->getKeyswitchDefinitions().size();
		for( int i=0; i<n; i++) {
			KeyswitchDefinition& keyswitchDef = patchDefinition->getKeyswitchDefinitions().at(i);
			if( keyswitchDef.key == key) {
				return &keyswitchDef;
			}
		}
	}
	return NULL;
}

/*
std::string Sampler::getKeySwitchName( int aType, int aParam)
{
	std::string name;
	int n=patchDefinition->getKeyswitchDefinitions().size();
	for( int i=0; i<n; i++) {
		KeyswitchDefinition& keyswitchDef = patchDefinition->getKeyswitchDefinitions().at(i);
		if( keyswitchDef.type == aType && keyswitchDef.param == aParam) {
			name =  keyswitchDef.name;
		}
	}
	return name;
}
*/

/*
 * 		MIDI NOTE HANDLING
 */

void Sampler::noteOn (const int midiChannel,
		const int midiNoteNumber,
		const float velocity)
{
	const ScopedLock sl (lock);

	if( patchDefinition == NULL)
		return;

    currentSoundSet = *pluginParameters->soundSetParam;
    
    //keyPressed[midiNoteNumber] = 1;

	if( isKeySwitch(midiNoteNumber)) {
		KeyswitchDefinition* currentKeySwitch = getKeySwitchDefinition( midiNoteNumber);
		if( currentKeySwitch != NULL) {
			if( currentKeySwitch->isPermanent) {
				switch( currentKeySwitch->type) {
					case KeyswitchDefinition::KEYSWITCHTYPE_SOUNDSET:
					{
						currentSoundSet = currentKeySwitch->param;
                        *pluginParameters->soundSetParam = currentSoundSet;
						break;
					}
					case KeyswitchDefinition::KEYSWITCHTYPE_SIMPLELEGATO:
					{
						*pluginParameters->legatoModeParam = (pluginParameters->legatoModeParam == 0 ? 1 : 0);
						break;
					}
				}
			} else {
				switch( currentKeySwitch->type) {
					case KeyswitchDefinition::KEYSWITCHTYPE_SOUNDSET:
					{
						currentSoundSet = currentKeySwitch->param;;
                        *pluginParameters->soundSetParam = currentSoundSet;
						break;
					}
					case KeyswitchDefinition::KEYSWITCHTYPE_SIMPLELEGATO:
					{
						*pluginParameters->legatoModeParam = 1;
						break;
					}
				}

			}
		}
	} else {
		int midiNote = midiNoteNumber -  *pluginParameters->playRangeShiftParam;
		if( midiNote < 0)
			midiNote = 0;
		if( midiNote > 127)
			midiNote = 127;
        
        
        


        int voicesStopped = 0;

		int monoGroup = keyMonoGroups[midiNote];

        if( *pluginParameters->monoModeParam ) {
                // MONOPHONIC MODE
                // Stop all sounding voices first.
                // If we are in legato,  set legato release time
                if( *pluginParameters->legatoModeParam == PatchDefinition::LEGATOMODE::SIMPLE) {
                    voicesStopped += stopAllNotes( midiChannel, *pluginParameters->legatoReleaseTParam);
                } else {
                    voicesStopped += stopAllNotes( midiChannel);
                }
        } else {
            if( monoGroup > -1 ) {
                // MONOGROUP MODE
                // If a monogroup nr > -1 is set for this note, stop all playing voices on the same
                // monogroup before playing this one.
                if( *pluginParameters->legatoModeParam == PatchDefinition::LEGATOMODE::SIMPLE) {
                    voicesStopped += stopAllNotesInMonoGroup( midiChannel, monoGroup, *pluginParameters->legatoReleaseTParam);
                } else {
                    voicesStopped += stopAllNotesInMonoGroup( midiChannel, monoGroup );
                }
            }
        }

		for (int i = sounds.size(); --i >= 0;)
		{
			SynthesiserSound* const soundSource = sounds.getUnchecked(i);
			XFadeSamplerSound* const sound = static_cast<XFadeSamplerSound* const> ( soundSource );

			if ( sound->appliesTo ( currentSoundSet, midiNote) && sound->appliesToChannel ( midiChannel ) )
			{


				if( ! *pluginParameters->monoModeParam ) {

					// POLYPHONIC MODE
					// If hitting a note that's still ringing, stop it first (it could be
					// still playing because of the sustain or sostenuto pedal).
					for ( int j = voices.size(); --j >= 0; )
					{
						XFadeSamplerVoice* const voice = (XFadeSamplerVoice* const)voices.getUnchecked (j);

						if ( voice->getCurrentlyPlayingNote() == midiNote
								&& voice->isPlayingChannel ( midiChannel )
								&& voice->isPlayingLayer(sound->soundDefinition->layer))
						{
							// If we are in legato,  set legato release time
							if( *pluginParameters->legatoModeParam == PatchDefinition::LEGATOMODE::SIMPLE) {
								voice->setOneHitReleaseS(  *pluginParameters->legatoReleaseTParam);
							}
							stopVoice ( voice, 1.0f, true );
                            voicesStopped++;
						}
					}
				}


				XFadeSamplerVoice* const voice = static_cast<XFadeSamplerVoice* const> (findFreeVoice ( sound, midiChannel, midiNote,     isNoteStealingEnabled() ));
            
                if( voice != nullptr) {
                    voice->setMonoGroup( monoGroup );
                    // if we are in legato and voices were playing, set the legato offset and attack time
                    voice->clearOneHits();
                    if( *pluginParameters->legatoModeParam == PatchDefinition::LEGATOMODE::SIMPLE && voicesStopped > 0) {
                        voice->setOneHitOffsetS( *pluginParameters->legatoOffsetTParam);
                        voice->setOneHitAttackS( *pluginParameters->legatoAttackTParam);
                    }
                    int soundingMidiNote = midiNote;

                    if (*(pluginParameters->pitchWheelModeParam) == PatchDefinition::PITCHWHEELMODE::SEMITONES_RETRIGGER) {
                        soundingMidiNote = midiNote + measuredPitchSemitones;
                    }
                    voice->setSoundingMidiNote( soundingMidiNote );
                    float beveledVelo = VelocityToGainMapper::applyBevel(patchDefinition->bevelRangeFromMidiNote, patchDefinition->bevelRangeToMidiNote, midiNoteNumber, *pluginParameters->bevelParam, velocity);
                    
                    if( beveledVelo > 0.0) {
                        startVoice ( voice, sound, midiChannel, midiNote , beveledVelo );
                    
                        if (*(pluginParameters->pitchWheelModeParam) == PatchDefinition::PITCHWHEELMODE::CONTINUOUS) {
                            Synthesiser::handlePitchWheel( midiChannel, currentPitchWheelValue);
                        } else if (*(pluginParameters->pitchWheelModeParam) == PatchDefinition::PITCHWHEELMODE::SEMITONES) {
                            float pwValue = calcPitchWheelFromSemiTones(measuredPitchSemitones, *(pluginParameters->pitchWheelRangeParam));
                            Synthesiser::handlePitchWheel( midiChannel, pwValue);
                        }
                    
                        switch( *(pluginParameters->pressureModeParam)) {
                            case PatchDefinition::PRESSUREMODE::VELOCITY:
                            case PatchDefinition::PRESSUREMODE::VELOCITYPRESSUREABS:
                                voice->calcXFade(beveledVelo);
                                break;
                            case PatchDefinition::PRESSUREMODE::PRESSUREABS:
                                voice->calcXFade( ((float)measuredPressure/127.0) );
                                break;
                        }
                    }
                }

			}
		}

	}

}

int Sampler::stopAllNotes( int midiChannel, float releaseT)
{
    if( patchDefinition == NULL)
        return 0;

    int voicesStopped = 0;
	for ( int j = voices.size(); --j >= 0; )
	{
		XFadeSamplerVoice* const voice = (XFadeSamplerVoice* const)voices.getUnchecked (j);
		if (  voice->isPlayingChannel ( midiChannel ) ) {
			voice->setOneHitReleaseS( releaseT);
			stopVoice ( voice, 1.0f, true );
            voicesStopped++;
		}
	}
    return voicesStopped;
}

int Sampler::stopAllNotes( int midiChannel)
{
    if( patchDefinition == NULL)
        return 0;

    int voicesStopped = 0;
	for ( int j = voices.size(); --j >= 0; )
	{
		XFadeSamplerVoice* const voice = (XFadeSamplerVoice* const)voices.getUnchecked (j);
		if (  voice->isPlayingChannel ( midiChannel ) ) {
			stopVoice ( voice, 1.0f, true );
            voicesStopped++;
		}
	}
    return voicesStopped;
}

int Sampler::stopAllNotesInMonoGroup( int midiChannel, int aMonoGroup, float releaseT)
{
    if( patchDefinition == NULL)
        return 0;

    int voicesStopped = 0;
	for ( int j = voices.size(); --j >= 0; )
	{
		XFadeSamplerVoice* const voice = (XFadeSamplerVoice* const)voices.getUnchecked (j);
		if (  voice->isPlayingChannel ( midiChannel ) && voice->isPlayingMonoGroup(aMonoGroup)) {
			voice->setOneHitReleaseS( releaseT);
			stopVoice ( voice, 1.0f, true );
            voicesStopped++;
		}
	}
    return voicesStopped;
}

int Sampler::stopAllNotesInMonoGroup( int midiChannel,int aMonoGroup )
{
    if( patchDefinition == NULL)
        return 0;
    
    int voicesStopped = 0;
	for ( int j = voices.size(); --j >= 0; )
	{
		XFadeSamplerVoice* const voice = (XFadeSamplerVoice* const)voices.getUnchecked (j);
		if (  voice->isPlayingChannel ( midiChannel ) && voice->isPlayingMonoGroup(aMonoGroup)) {
			stopVoice ( voice, 1.0f, true );
		}
        voicesStopped++;
	}
    return voicesStopped;
}


void Sampler::noteOff (int midiChannel,
		int midiNoteNumber,
		float velocity,
		bool allowTailOff)
{
    if( patchDefinition == NULL)
        return;

	if( isKeySwitch(midiNoteNumber)) {
		KeyswitchDefinition* keySwitch = getKeySwitchDefinition( midiNoteNumber);
		if( keySwitch != NULL) {
			if( !keySwitch->isPermanent) {
				switch( keySwitch->type) {
					case KeyswitchDefinition::KEYSWITCHTYPE_SOUNDSET:
					{
						currentSoundSet = 0;
                        *pluginParameters->soundSetParam = currentSoundSet;
						break;
					}
					case KeyswitchDefinition::KEYSWITCHTYPE_SIMPLELEGATO:
					{
						*pluginParameters->legatoModeParam = 0;
						break;
					}
				}
			}
		}
		Synthesiser::noteOff( midiChannel, midiNoteNumber, velocity, allowTailOff);

	} else {

		int midiNote = midiNoteNumber -  *pluginParameters->playRangeShiftParam;
		if( midiNote < 0)
			midiNote = 0;
		if( midiNote > 127)
			midiNote = 127;
        
        

		Synthesiser::noteOff( midiChannel, midiNote, velocity, allowTailOff);
	}
}





/*
 * 			PITCHWHEEL HANDLING
 */


/*
    Handle semitone pitchwheel
>>The value 8192 (sent, LSB first, as 0x00 0x40), is centered, or "no pitch bend."
The value 0 (0x00 0x00) means, "bend as low as possible," and, similarly,
16383 (0x7F 0x7F) is to "bend as high as possible."<<

==>  0..8192 <= lower
     8192-16383 >= higher

     pbVal = value - 8192
     if( pbVal < 0 )
     	  st = pbVal / 8192 * pitchBendRangeSt
     else
     	  st = pbVal / 8191 * pitchBendRangeSt

*/

void Sampler::retriggerAllNotes( int midiChannel )
{
	retriggerNotes.clear();
    std::stringstream log1;
    log1 << "\nTO RETRIGGER  nVoices=" << voices.size();
    Logger::getCurrentLogger()->writeToLog(log1.str());
    
	for ( int j = voices.size(); --j >= 0; )
	{
		XFadeSamplerVoice* const voice = (XFadeSamplerVoice* const)voices.getUnchecked (j);
        if ( voice->isPlayingChannel ( midiChannel ) && voice->getCurrentlyPlayingNote() > -1 && !voice->isPlayingButReleased() && !voice->isEnvInRelease() )
        {
			RetriggerNoteEntry rne;
            rne.sound = voice->getSound();
			rne.midiNote = voice->getMidiNoteNumber();
			rne.layer = voice->getLayerNumber();
			rne.velocity = voice->getVelocity();
			retriggerNotes.push_back( rne );
			voice->setOneHitReleaseS(   *(pluginParameters->pitchWheelRetriggerReleaseTParam) );
            
            stopVoice ( voice, 1.0f, true );
            //Synthesiser::noteOff( midiChannel, voice->getMidiNoteNumber(), voice->getVelocity(), true);
        }
    }
    int n= retriggerNotes.size();
    for( int j=0; j<n; j++) {
        
        RetriggerNoteEntry& rne = retriggerNotes[j];

        int soundingMidiNote = rne.midiNote + measuredPitchSemitones;
        for (int i = sounds.size(); --i >= 0;)
        {
            SynthesiserSound* const soundSource = sounds.getUnchecked(i);
            XFadeSamplerSound* const sound = static_cast<XFadeSamplerSound* const> ( soundSource );
            
            
            if ( sound->appliesTo ( currentSoundSet, soundingMidiNote) && sound->appliesToChannel ( midiChannel ) )
            {
                XFadeSamplerVoice* const voice = static_cast<XFadeSamplerVoice* const> (findFreeVoice ( rne.sound, midiChannel, soundingMidiNote, true ));
                voice->setSoundingMidiNote( soundingMidiNote );
                voice->clearOneHits();
                voice->setOneHitOffsetS( *(pluginParameters->pitchWheelRetriggerOffsetTParam));
                voice->setOneHitAttackS( *(pluginParameters->pitchWheelRetriggerAttackTParam));
                startVoice ( voice, rne.sound, midiChannel, rne.midiNote , rne.velocity );
            
                switch( *(pluginParameters->pressureModeParam)) {
                    case PatchDefinition::PRESSUREMODE::VELOCITY:
                    case PatchDefinition::PRESSUREMODE::VELOCITYPRESSUREABS: {
                        voice->calcXFade(rne.velocity);
                        break;
                    }
                    case PatchDefinition::PRESSUREMODE::PRESSUREABS:
                        voice->calcXFade( ((float)measuredPressure/127.0) );
                        break;
                }
                break;
            }

        }
	}


}

int Sampler::calcSemitonesFromPitchWheel( int pitchWheelValue, int pitchBendRangeSt )
{
	float pbVal = pitchWheelValue - 8192.0;
	int st = 0;
	if( pbVal < 0 ) {
		st = (int)(pbVal / 8192.0 * pitchBendRangeSt);
	} else {
		st = (int)(pbVal / 8191.0 * pitchBendRangeSt);
	}
	return st;
}

int Sampler::calcPitchWheelFromSemiTones( int semiTones, int pitchBendRangeSt )
{
    int pw = 8192;
    if( semiTones < 0) {
        pw = ((float)semiTones) * 8192.0 / pitchBendRangeSt + 8192;
    }  else {
        pw = ((float)semiTones) * 8191.0 / pitchBendRangeSt + 8192;
        
    }
    return pw;
}



void Sampler::handlePitchWheel (int midiChannel, int wheelValue)
{
    if( patchDefinition == NULL)
        return;
    
    currentPitchWheelValue = wheelValue;

	switch( *(pluginParameters->pitchWheelModeParam) ) {
		case PatchDefinition::PITCHWHEELMODE::CONTINUOUS:
			Synthesiser::handlePitchWheel( midiChannel, wheelValue);
			break;
		case PatchDefinition::PITCHWHEELMODE::SEMITONES:
		{
			measuredPitchSemitones = calcSemitonesFromPitchWheel( wheelValue, *(pluginParameters->pitchWheelRangeParam));
			if( measuredPitchSemitones != previousMeasuredPitchSemitones) {
                int pw = calcPitchWheelFromSemiTones( measuredPitchSemitones, *(pluginParameters->pitchWheelRangeParam));
				Synthesiser::handlePitchWheel( midiChannel, pw);
			}
			previousMeasuredPitchSemitones = measuredPitchSemitones;
		}
		break;
		case PatchDefinition::PITCHWHEELMODE::SEMITONES_RETRIGGER:
		{
			measuredPitchSemitones = calcSemitonesFromPitchWheel( wheelValue,  *(pluginParameters->pitchWheelRangeParam) );
			if( measuredPitchSemitones != previousMeasuredPitchSemitones) {
				retriggerAllNotes( midiChannel );
			}
			previousMeasuredPitchSemitones = measuredPitchSemitones;
		}
		break;
	}
}	




/*
 * 			MIDI CONTOROLLER HANDLING
 */

void Sampler::handleController (int midiChannel,
		int controllerNumber,
		int controllerValue)
{
    if( patchDefinition == NULL)
        return;

	if(  controllerNumber == *(pluginParameters->pressureControllerParam) && *(pluginParameters->pressureModeParam) != PatchDefinition::PRESSUREMODE::VELOCITY) {
        // keep the measured pressure, so we can use it in noteOn
		measuredPressure = controllerValue;
		float fMeasuredPressure = ((float)measuredPressure/127.0f);
		if( *(pluginParameters->pressureModeParam) == PatchDefinition::PRESSUREMODE::PRESSUREABS) {
			for (int i = voices.size(); --i >= 0;)
			{
				XFadeSamplerVoice* const voice = (XFadeSamplerVoice* const)voices.getUnchecked (i);

				if (midiChannel <= 0 || voice->isPlayingChannel (midiChannel))
					voice->pressureMoved ( fMeasuredPressure);
			}
		} else if ( *(pluginParameters->pressureModeParam) == PatchDefinition::PRESSUREMODE::VELOCITYPRESSUREABS) {
			for (int i = voices.size(); --i >= 0;)
			{
				XFadeSamplerVoice* const voice = (XFadeSamplerVoice* const)voices.getUnchecked (i);

				if (midiChannel <= 0 || voice->isPlayingChannel (midiChannel)) {
					if( measuredPressure > previousMeasuredPressure) {
						voice->pressureCatchUp(fMeasuredPressure);
					} else {
						voice->pressureCatchDown(fMeasuredPressure);
					}
				}
			}

		}

		previousMeasuredPressure = measuredPressure;
	} else {
        // forward controller handling
		Synthesiser::handleController(midiChannel, controllerNumber, controllerValue);
	}
}






