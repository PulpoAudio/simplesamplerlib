/******************************************************************
 This software has been developed 2017 by Rudi Leitner
 as open source under the GPL3 license.
 
 It uses the open source version of JUCE GRAPEFRUIT under GPL3.
 *******************************************************************/
//
//  DFDThreadPool.cpp
//  SimpleSamplerComponent
//
//  Created by Rudolf Leitner on 31/12/16.
//
//

#include "DFDThreadPool.hpp"
#include <sstream>







//double SampleLoader::diskUsage = 0;
//double SampleLoader::lastCallToRequestData = 0;




SampleLoader::SampleLoader(ThreadPool *pool_, SampleBinSample* aBinSample, DFDRingBuffer* aRingBuffer, int aRingBufferStartNdx, int aReadFromFrameNdx, int aNrOfFramesToRead)
:ThreadPoolJob("SampleLoader"),backgroundPool(pool_)
{
	ringBuffer = aRingBuffer;
	ringBufferStartNdx = aRingBufferStartNdx;
	readFromFrameNdx = aReadFromFrameNdx;
	nrOfFramesToRead = aNrOfFramesToRead;
	binSample = aBinSample;
}

/*void SampleLoader::reset() {
    diskUsage = 0;
    lastCallToRequestData = 0;
}

double SampleLoader::getDiskUsage() {
    const double returnValue = diskUsage;
    diskUsage = 0.0;
    return returnValue;
}*/

ThreadPoolJob::JobStatus SampleLoader::runJob()
{
    //const ScopedLock sl(cs);
    /*std::stringstream log1;
    log1 << "Loader runJob.  RingBufStartNdx=" << ringBufferStartNdx << "  nrOfFramesToRead=" << nrOfFramesToRead << "  readFromFrameNdx=" << readFromFrameNdx << "\n";
    Logger::getCurrentLogger()->writeToLog(log1.str());
    */
    //const double readStart = Time::highResolutionTicksToSeconds(Time::getHighResolutionTicks());
    
    
    
    if( binSample->memoryMappedReader != NULL && ringBuffer != NULL) {

        if( ringBufferStartNdx == 0) {
            ringBuffer->buf0Ready = false;
        } else {
            ringBuffer->buf1Ready = false;
        }

        binSample->memoryMappedReader->read(&(ringBuffer->buffer), ringBufferStartNdx, nrOfFramesToRead, readFromFrameNdx, true, (binSample->numberOfChannels == 2));
        
        if( ringBufferStartNdx == 0) {
            ringBuffer->buf0Ready = true;
        } else {
            ringBuffer->buf1Ready = true;
        }
        
        
    }
    
    /*const double readStop = Time::highResolutionTicksToSeconds(Time::getHighResolutionTicks());
    const double readTime = (readStop - readStart);
    const double timeSinceLastCall = readStop - lastCallToRequestData;
    const double diskUsageThisTime =  readTime / timeSinceLastCall;
    diskUsage = jmax(diskUsage, diskUsageThisTime);
    lastCallToRequestData = readStart;
    */
    
    /*std::stringstream log2;
    log2 << "Loader runJob finished.  RingBufStartNdx=" << ringBufferStartNdx << "  nrOfFramesToRead=" << nrOfFramesToRead << "  readFromFrameNdx=" << readFromFrameNdx << "\n";
    Logger::getCurrentLogger()->writeToLog(log2.str());
    */
    
    return JobStatus::jobHasFinished;
    
}

